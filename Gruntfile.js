"use strict";

var grunt = require('grunt');

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		version: {
			release: {
				src: ['package.json','public/js/app/main.js'],
				options: {
					release: 'patch'
				}
			}
		},
		htmlConvert: {
			options: {
			},
			templates: {
				src: [
					'public/js/app/view/*.html'
				],
				dest: 'build/views.min.js'
			}
		},
		concat: {
			options: {
				// separator: ';\n'
			},
			main: {
				src: [
					// 'public/js/lib/jquery/jquery.js',
					// 'public/js/lib/moment.js',
					// 'public/js/lib/toastr.js',
					// 'public/js/app/utils/utils.js',
					// 'public/js/lib/jquery/jquery-ui.min.js',
					// 'public/js/lib/angular/angular.min.js',
					// 'public/js/lib/angular/angular-route.min.js',
					// 'public/js/app/main.js',
					// 'public/js/controllers/*.js',
					// 'public/js/models/*.js',
					// 'public/js/directives/*.js',
					// 'public/js/services/*.js'
					// 'public/js/*.js'
					'public/jss/general.js',
					'public/jss/mine.js',
					'public/jss/fiveinrow.js'
				],
				dest: 'public/Jss/gamesZone.js'
			},
			css: {
				src: [
					'public/styles/*.css'
				],
				dest: 'build/main.css'
			}
		},
		cssmin: {
			combine: {
				files: {
					'build/main.min.css': [
						'build/main.css'
					],'public/radio_files/radio.min.css': [
						'public/radio_files/radioAds.css'
					]
				}
			}
		},
		uglify: {
			main: {
				files: {
					'build/main.min.js': [ 'build/main.js' ],
					'public/radio_files/games.min.js': [ 'public/Jss/gamesZone.js' ],
					'public/radio_files/radio.min.js': [ 'public/radio_files/radio.js' ]
				},
				options: {
					mangle: true
				}
		  	}
		},
		clean: {
			temp: {
				src: ['build/views.min.js']
			}
		},
		watch: {
			dev: {
				files: [
					'public/radio_files/*', 'public/jss/*', '!public/radio_files/*min*'
				],
				// tasks: [ 'htmlConvert:templates', 'concat:main',  'copy:main', 'concat:css', 'clean:temp' ],
				tasks: [ 'concat:main',  'copy:main', 'concat:css', 'cssmin', 'uglify:main' ],
				options: {
					atBegin: true
				}
			}
		},
		connect: {
			server: {
				options: {
					port: 9999
				}
			}
		},
		sass: {
			dist: {
			  files: [{
			    expand: true,
			    cwd: 'css',
			    src: ['**/*.scss'],
	      		dest: 'css/',
			    ext: '.css'
			  }]
			}
		},
  		copy: {
		  main: {
		    files: [
		      {expand: true, cwd: 'public/radio_files/ads_images/',src: ['**'], dest: 'build/images', filter: 'isFile'},
		    //   {expand: true, cwd: 'public/',src: ['index.html'], dest: 'build/', filter: 'isFile'},
		      // {expand: true, src: ['public/index.html'], dest: 'build/', filter: 'isFile'},
		    ],
		  },
		  dev: {
		    files: [
				{
					expand: true,
					cwd: 'build/',
					src: ['main.js'],
					dest: 'build/',
            		rename: function(dest, src) {
              			return dest + src.substring(0, src.indexOf('.')) + '-debug.js';
            		}
        		},
				{
					expand: true,
					cwd: 'build/',
					src: ['main.css'],
					dest: 'build/',
            		rename: function(dest, src) {
              			return dest + src.substring(0, src.indexOf('.')) + '-debug.css';
            		}
        		}
		    ]
		  }
		},
		karma: {
			unit: {
				configFile: 'karma.conf.js'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-html-convert');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-version');
	// grunt.loadNpmTasks('grunt-contrib-sass');
	// grunt.loadNpmTasks('grunt-karma');

	// grunt.registerTask('dev', ['connect:server', 'watch:dev' ]);
	//grunt.registerTask('dev',  ['watch:dev']);
	grunt.registerTask('dev',  [ 'concat:main',  'copy:main', 'concat:css', 'cssmin', 'uglify:main']);
	grunt.registerTask('package', ['htmlConvert:templates', 'concat:main',  'concat:css', 'copy:dev', 'cssmin', 'uglify:main', 'copy:main', 'clean:temp']);
	grunt.registerTask('release', ['version:release', 'htmlConvert:templates', 'concat:main',  'sass:dist', 'concat:css', 'copy:dev', 'cssmin', 'uglify:main', 'copy:main', 'clean:temp']);
	// grunt.registerTask('test', ['karma:unit']);

module.exports = grunt;
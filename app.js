'use strict';

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var radioRoute = require('./routes/radio');

var app = express();
const radio = require('./lib/system/radio-global');
radio.appRoot = __dirname;
radio.expressAppContext = app;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

app.get('/*-icon-*', function (req, res) {
    res.setHeader('Content-Type', 'image/png');
    res.sendFile(path.join(__dirname, 'public/stylesheets/radio.png'));
});
app.get('/radio_files/playstation-*', function (req, res) {
    res.setHeader('Content-Type', 'image/png');
    res.sendFile(path.join(__dirname, 'public/stylesheets/playstation.png'));
});

app.use('/:stationKey', radioRoute.stations);
app.use('/', radioRoute.stations);

// app.use('/home2', function (req, res, next) {
//    res.render('nettab');
// });

// app.use('/home', function (req, res, next) {
//    res.render('webtop');
// });


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;

const util = require('util');
const radio = require('./radio-global');
const logger = radio.appContext.logger.getLogger();

/**
 *
 * @constructor
 * This class is inherited by all actions
 *
 */
function RdRequestAction() {
    this.req = this.req || arguments[0];
    this.res = this.res || arguments[1];
}

util.inherits(RdRequestAction, radio.appContextProto);
/**
 *
 * @param successCb
 * @param errorCb
 */
RdRequestAction.prototype.validate = function (successCb, errorCb) {
    var _root = this;
    radio.appContext.itSessionMgr.gateKeeper(_root.req, _root.res, _root.permissions, {
        success: function (sessionData) {
            logger.info(_root.constructor.name, 'success');
            successCb(sessionData);
        },
        error: function (error) {
            logger.info(_root.constructor.name, 'failure');
            errorCb(error || {
                    errorMessage: radio.appContext.localStrings("unknown api request")
                });
        }
    }, this.isLogin);
};
/**
 * Return to the server the data as json
 * @param data
 */
RdRequestAction.prototype.successJSON = function (data) {
    if(typeof data === "undefined"){
        data = {};
    }
    data.status = true;
    try {
        this.res.json(data);
    } catch (e) {
        logger.error("Failed to send successJSON", e);
    }
};
/**
 * Return to the server the error message as json
 * @param data
 */
RdRequestAction.prototype.errorJSON = function (data, e) {
    if(typeof data === "undefined"){
        data = {};
    }
    data.status = false;
    logger.error(data.errorMessage, e);
    this.res.json(data);
};
/**
 * Render the jade template and return it to the client
 * @param name - the jade template name
 * @param options - the jade template params *
 */
RdRequestAction.prototype.renderHtml = function (name, options) {
    options = options || {};
    options.APP_FLODER = radio.appFolder;
    this.res.render(name, options);
};
/**
 * the default values
 * permissions - view all pages
 * isLogin - is Login page
 */
RdRequestAction.prototype.permissions = [];
RdRequestAction.prototype.isLogin = false;

module.exports = RdRequestAction;
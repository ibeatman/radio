const crypto = require('crypto');

module.exports = function (schema, rdSessionMgr) {

    rdSessionMgr.setLoginFunction(function (username, password, success, failure) {
        try {
            var _root = this;
            if (!username || !password)
                throw new Error("Please fill all fields.");

            var users = schema.users;
            users.getComplex({
                limit: '0,1',
                conditions: [
                    ['email', '=', username],
                    'AND',
                    ['password', '=', crypto.createHash('md5').update(password).digest("hex")]
                ]
            }, function (dbResponse) {
            try {
                    if (dbResponse.length === 0) {
                        failure("Wrong username or password.");
                    } else {
                        for (var i in dbResponse[0]._data) {
                            if (dbResponse[0]._data.hasOwnProperty(i)) {
                                _root.add(i, dbResponse[0]._data[i]);
                            }
                        }
                        _root.userLoggedin('login');
                        success(dbResponse[0]);
                    }
                } catch (e) {
                    failure(e.message);
                }
            });
        } catch (e) {
            failure(e.message);
        }
    });
    
    rdSessionMgr.setSsoFunction(function (userid, cb) {
        try {
            var _root = this;
            if (!userid)
                cb("userid is undefined.");

            var users = schema.users;
            users.getComplex({
                limit: '0,1',
                conditions: [
                    ['id', '=', userid]
                ]
            }, function (dbResponse) {
            try {
                    if (dbResponse.length === 0) {
                        cb("Wrong username or password.");
                    } else {
                        for (var i in dbResponse[0]._data) {
                            if (dbResponse[0]._data.hasOwnProperty(i)) {
                                _root.add(i, dbResponse[0]._data[i]);
                            }
                        }
                        _root.userLoggedin('login');
                        cb(dbResponse[0]);
                    }
                } catch (e) {
                cb(e.message);
                }
            });
        } catch (e) {
            cb(e.message);
        }
    });
};
var SessionMgr = require("./session-mgr").SessionMgr;

/**
 * Store all it sessions in the global name space
 * @type {{}}
 */
global.sessions = global.sessions || {};


global.sessionMgr = null;

/**
 * Session garbage collector
 */
var sessionGrabageCollector = false;

function validateSession(clientSesId, callback) {
    setTimeout(function () {
        if (global.sessions[clientSesId] && global.sessions[clientSesId].permissions) {
            if (global.sessions.hasOwnProperty(clientSesId) && global.sessions[clientSesId].deleteMe) {
                global.sessionMgr.destroy(clientSesId);
                delete global.sessions[clientSesId];
            } else if ((Date.now() - global.sessions[clientSesId].sessionTtl) > (global.sessionMgr.sessionExp * 1000)) {
                global.sessionMgr.prepareToDestroy(clientSesId);
            } else if ((Date.now() - global.sessions[clientSesId].sessionTtl) > (global.sessionMgr.sendMsgSessionExp * 1000)) {
                global.sessionMgr.alertDestruction(clientSesId);
            }
        } else {
            // global.sessionMgr.emit("log", "SessionIdk [" + clientSesId + "] is not logged in");
        }
        if (callback) callback();
    }, 1);
}

setInterval(function () {
    if (!sessionGrabageCollector && global.sessions) {
        var keys = Object.keys(global.sessions);
        var totalSessions = keys.length;
        sessionGrabageCollector = totalSessions > 0;
        for (var i = 0; i < totalSessions; i++) {
            if (i === totalSessions - 1) {
                validateSession(keys[i], function () {
                    sessionGrabageCollector = false;
                });
            } else {
                validateSession(keys[i]);
            }
        }
    }
}, 1000);


module.exports = {
    sessionMgr: function (conf, permissionsMgr) {
        if (!global.sessionMgr) {
            global.sessionMgr = new SessionMgr(conf, permissionsMgr);
        }
        return global.sessionMgr;
    }
};
const util = require('util');
const EventEmitter = require('events');

/**
 * Main session object, holds all session data, permissions & session ttl
 * @param sessionId - string
 * @constructor
 */
function RdSession(sessionId) {
    EventEmitter.call(this);
    /**
     * The session data from the database
     * @type {{}}
     * @private
     */
    this._data = {};
    this._sessionId = sessionId;
    /**
     * Will be array, initialize as null to make sure a session was started
     * @type {null}
     */
    this.permissions = null;
    /**
     * Last request time for this session, updated on each gateKeeper
     * @type {number}
     */
    this.sessionTtl = Date.now();

    this.mgr = null;

    this.webSocketConn = null;
}
util.inherits(RdSession, EventEmitter);
/**
 * Add data to _data, each property emits a "set" event
 * @param key - string
 * @param newVal - mixed (optional)
 */
RdSession.prototype.add = function (key, newVal) {
    var _root = this;
    // do not try to define the key getter/seeter twice 
    if (!_root._data.hasOwnProperty(key)) {
        Object.defineProperty(_root, key, {
            set: function (value) {
                _root.emit('set', {
                    sessionId: _root._sessionId,
                    oldVal: _root._data[key],
                    newVal: value
                });
                _root._data[key] = value;
            },
            get: function () {
                return _root._data[key];
            }
        });
    }
    if (typeof newVal !== 'undefined') {
        _root[key] = newVal;
    }
};

/**
 * Deletes a property of _data, each deletion emits "remove" event
 * @param key - string
 */
RdSession.prototype.remove = function (key) {
    if (this._data[key]) {
        delete this._data[key];
        this.emit('remove', key);
    }
};

/**
 * Execute the login function registered to the ItSessionMgr object
 * @param username string
 * @param password string
 * @param success func
 * @param failure func
 */
RdSession.prototype.login = function (username, password, success, failure) {
    this.mgr.loginFunc.call(this, username, password, success, failure);
};

/**
 * Execute the sso function registered to the ItSessionMgr object
 * @param userid string
 * @param password string
 * @param success func
 * @param failure func
 */
RdSession.prototype.SsoFunc = function (userid, cb) {
    this.mgr.SsoFunc.call(this, userid, cb);
};

/**
 * Add the user permissions to the user session after login
 */
RdSession.prototype.userLoggedin = function () {
    this.setUserPermissions();

    //This indicate that the user is logged in but the SSO cookie maybe not exists 
    this.justLoggedin = true;
    var _root = this;
    setTimeout(function () {
        //the SSO cookie should be exists now 
        _root.justLoggedin = false;
    }, 1000);

    //check for admin downtime for a new logged in user
    this.notifyAdminDowntime();
};

RdSession.prototype.notifyAdminDowntime = function () {
    if (this.mgr.shutdownAt && this.mgr.shutdownAt > Date.now()) {
        var _root = this;
        setTimeout(function () {
            _root.mgr.emit('notifyAdminDowntime', _root._sessionId, ['login'], {
                message: _root.mgr.shutdownMessage,
                shutdownIn: ((_root.mgr.shutdownAt - Date.now()) / 60000)
            })
        }, 10000);
    }
};

RdSession.prototype.setUserPermissions = function () {
    // cloning the current user's permissions
    this.permissions = this.mgr.permissionsMgr.allPermissionsData.usersPermissions[this.id].slice(0);
    this.strictPermissions = this.mgr.permissionsMgr.allPermissionsData.strictPermissions;
    this.permissions.push('login');
    var _root = this;
    this.userRoles = [];
    this.mgr.permissionsMgr.allPermissionsData.userRoles[this.id].forEach(function (userRole, index) {
        _root.userRoles.push(_root.mgr.permissionsMgr.allPermissionsData.roles[userRole.role_id].name);
    });
    this.mgr.emit("log", [
        this.email,
        ' - sessionId:',
        this._sessionId,
        ' - Roles:',
        this.userRoles.join(","),
        ' - Permissions:',
        this.permissions.join(",")
    ].join(' '));
};

RdSession.prototype.addPermissions = function (permission) {
    if (this.permissions === null) {
        this.permissions = [permission];
    } else if (this.permissions.indexOf(permission) === -1) {
        this.permissions.push(permission);
    }
};

RdSession.prototype.hasPermissions = function (perms) {

    perms = Array.isArray(perms) ? perms : [perms];
    var hasPermission = false;
    var _root = this;
    // Always allow superusers

    var isStrict = perms.some(function (perm) {
        return _root.strictPermissions.indexOf(perm) > -1;
    });

    if (this.hasRoles('superadmin') && !isStrict) {
        hasPermission = true;
    }

    if (!hasPermission) {
        if (perms.length === 0) {
            hasPermission = true;
        } else {
            hasPermission = perms.every(function (permission) {
                return _root.permissions.indexOf(permission) > -1;
            });
        }
    }

    this.mgr.emit("log", [
        "Check permission '",
        perms.join(','),
        "' In - " + this.permissions.join(","),
        "==>",
        hasPermission ? "Have Permission" : "Don't Have Permission"
    ].join(' '));

    return hasPermission;
};

/**
 * Checking user roles
 * @param roles
 * @returns {boolean} true - all roles exists otherwise false
 */
RdSession.prototype.hasRoles = function (roles) {
    roles = Array.isArray(roles) ? roles : [roles];
    var hasRoles = false;
    if (roles.length === 0) {
        hasRoles = true;
    } else {
        var _root = this;
        hasRoles = roles.every(function (role) {
            return _root.userRoles.indexOf(role) > -1;
        });
    }

    return hasRoles;
};

function intersectArray(a, b) {
    var ai = 0, bi = 0;
    var result = [];

    while (ai < a.length && bi < b.length) {
        if (a[ai] < b[bi]) {
            ai++;
        }
        else if (a[ai] > b[bi]) {
            bi++;
        }
        else /* they're equal */
        {
            result.push(a[ai]);
            ai++;
            bi++;
        }
    }

    return result;
}

module.exports = {
    RdSession: RdSession
};
const util = require('util');
const EventEmitter = require('events');
const randomstring = require("randomstring");
const requestIp = require('request-ip');
const crypto = require('crypto');
var RdSession = require('./rd-session').RdSession;

function getSessionFromGlobalStorage(sessionId) {
    return global.sessions[sessionId];
}

function setSessionInGlobalStorage(sessionId, itSession) {
    return global.sessions[sessionId] = itSession;
}

/**
 * Main session manager
 * gets the session configuration params from configuration file
 * @param conf
 * @constructor
 */
function SessionMgr(conf, permissionsMgr) {
    EventEmitter.call(this);

    /**
     * Secret session id string for the session id hash
     */
    this._secret = randomstring.generate(32);
    /**
     * Reference to a injected login function
     * @type {function}
     */
    this.loginFunc = null;
    /**
     * Define when remove the session from the session manager (default value is 30 min)
     * @type {number}
     */
    this.sessionExp = conf.sessionExp || 1800;
    /**
     * Define when display the user a message that the session is about to expire (defalut value 25 min)
     * @type {number}
     */
    this.sendMsgSessionExp = conf.sendMsgSessionExp || 1500;


    this.permissionsMgr = permissionsMgr;


    this.sso = conf.sso || {
            "key": "ssoprivatekey",
            "cookieName": "rdsso",
            "adminDomain": "radio.co.il",
            "expirationTime": 1800,
            "active": false
        };
}

util.inherits(SessionMgr, EventEmitter);
/**
 * Starts an ItSession
 * @param req
 * @param res
 * @returns {boolean || ItSession}
 */
SessionMgr.prototype.start = function (req, res, cb) {
    var _root = this;
    try {
        /**
         * Add a new ItSession object to the global session storage
         */
        var createNewSession = function (callBack) {
            var sessionId = _root.generateSessionId(req);
            rdSession = new RdSession(sessionId);
            res.clearCookie('token');
            res.cookie('token', sessionId);
            setSessionInGlobalStorage(sessionId, rdSession);
            rdSession.mgr = _root;
            rdSession.ip = requestIp.getClientIp(req);
            _root.emit("log", "****   rdSession.ip = " + rdSession.ip);
            rdSession.userAgent = req.headers['user-agent'];
            _root.checkForSso(req, rdSession, function () {
                callBack(rdSession);
            });
        };

        var rdSession = getSessionFromGlobalStorage(req.cookies.token);
        if (!rdSession || !this.validateSessionId(rdSession, req)) {
            createNewSession(function (itSession) {
                cb(itSession);
            });
        } else {
            rdSession.sessionTtl = Date.now();
            if (rdSession.id) {
                _root.addSso(req, res, rdSession, function (isSuccess) {
                    if (isSuccess) {
                        cb(rdSession);
                    } else {
                        cb(false);
                    }
                });
            } else {
                _root.checkForSso(req, rdSession, function () {
                    cb(rdSession);
                });
            }
        }

    } catch (e) {
        console.error("Failed to start a session", e, e.stack.split('\n'));
        cb(false);
    }
};
/**
 * Destroying the current session
 * @param req
 */
SessionMgr.prototype.logout = function (itSession, res, callback) {
    var sessionId = itSession._sessionId;

    this.emit("log", "User loggedout with session - " + sessionId);
    this.prepareToDestroy(sessionId);
    res.cookie(this.sso.cookieName, '', {
        maxAge: 0,
        domain: this.sso.adminDomain
    });
    callback && callback();
};

/**
 * A request wrapper method to validate a user session
 * @param req
 * @param res
 * @param permissions
 * @param callback
 */
SessionMgr.prototype.gateKeeper = function (req, res, permissions, callbacks, isLogin) {
    var _root = this;

    this.start(req, res, function (session) {
        var ok = session !== false;
        if (ok && permissions.length > 0 && !isLogin) {
            if (!session.permissions) {
                ok = false;
            } else {
                permissions.some(function (permission) {
                    if (!session.hasPermissions(permission)) {
                        ok = false;
                        return true;
                    }
                });
            }
        }
        if (ok) {
            callbacks.success(session);
        }
        else {
            res.redirect('/login');
        }
    });
};
/**
 * Reset the timer for session experiation
 * @param sessionId
 */
SessionMgr.prototype.restartSessionTtl = function (sessionId) {
    if (global.sessions[sessionId]) {
        global.sessions[sessionId].sessionTtl = Date.now();
        this.emit("closeSessionAlertDestruct", sessionId, ['login']);
    }
};
/**
 * Mark a session for deletion
 * @param sessionId
 */
SessionMgr.prototype.prepareToDestroy = function (sessionId, cb) {
    this.emit("log", "prepareToDestroy session - " + sessionId);
    if (global.sessions[sessionId]) {
        global.sessions[sessionId].deleteMe = true;
    }
    cb && cb();
};
/**
 * Session destroied
 * @param sessionId
 */
SessionMgr.prototype.destroy = function (sessionId) {
    // send message to client to reload page
    // from sendMessageToClient in server-alerts.js
    this.emit("sessionDestruct", sessionId, ['login']);
};
/**
 * Issue an destruction alert
 * @param sessionId
 */
SessionMgr.prototype.alertDestruction = function (sessionId) {
    this.emit("sessionAlertDestruct", sessionId, ['login'], {exp: (this.sessionExp - this.sendMsgSessionExp)});
};
/**
 * Creates an MD5 of the user agent, ip tnd this._secret
 * @param req
 * @returns {*}
 */
SessionMgr.prototype.generateSessionId = function (req) {
    var userAgent = req.headers['user-agent'];
    var ip = requestIp.getClientIp(req);
    var sessionId = crypto.createHash('md5').update(this._secret + userAgent + ip + Date.now()).digest("hex");
    this.emit("log", "generateSessionId [" + sessionId + "] with - " + this._secret + userAgent + ip + Date.now());
    return sessionId;
};

/**
 * Handle case when seting the sso.active from ad_global_preferences
 * @returns {boolean}
 */
SessionMgr.prototype.isSsoActive = function () {
    var retVal = this.sso.active;

    if(typeof this.sso.active != "boolean"){
        retVal = this.sso.active === "true";
    }

    return retVal;
}
/**
 *
 * Checking for existing cookies and login with the userId if exists
 * @param req
 * @param itSession
 * @param cb
 */
SessionMgr.prototype.checkForSso = function (req, itSession, cb) {
    var textToDecipher = req.cookies[this.sso.cookieName];
    var dec = '';
    if (textToDecipher && this.isSsoActive()) {
        var privateKey = this.sso.key;

        var iv = new Buffer(8);
        iv.fill(0);

        var decipher = crypto.createDecipheriv('des-cbc', privateKey.substr(0, 8), iv);
        var cookieValue = decipher.update(textToDecipher, 'base64', 'utf8');
        cookieValue += decipher.final('utf8');
        if (cookieValue) {
            try {
                this.emit("log", "===> User (read) cookieValue - " + cookieValue);
                cookieValue = JSON.parse(cookieValue);
                if (cookieValue.ex > Date.now() && itSession.userAgent === cookieValue.ua && itSession.ip === cookieValue.ip) {
                    // Get user data by UserId
                    itSession.SsoFunc(cookieValue.ui, function () {
                        cb();
                    });
                } else {
                    cb();
                }
            } catch (ex) {
                cb();
            }
        } else {
            cb();
        }
    } else {
        cb();
    }
};

/**
 * Add SSO cookie
 * @param req
 * @param res
 * @param itSession
 * @param cb
 */
SessionMgr.prototype.addSso = function (req, res, itSession, cb) {
    try {
        if (this.isSsoActive()) {

            var privateKey = this.sso.key;
            var iv = new Buffer(8);
            iv.fill(0);

            var textToDecipher = req.cookies[this.sso.cookieName];
            if (textToDecipher) {
                var decipher = crypto.createDecipheriv('des-cbc', privateKey.substr(0, 8), iv);
                var cookieValue = decipher.update(textToDecipher, 'base64', 'utf8');
                cookieValue += decipher.final('utf8');
                var userData = {};
                if (cookieValue) {
                    try {
                        userData = JSON.parse(cookieValue);
                    } catch (ex) {
                    }
                }
            }
            if ((itSession.justLoggedin && !textToDecipher) || 
                (textToDecipher && userData && (userData.ui === itSession.id && userData.ip === itSession.ip))) {
                var data = {
                    "ip": itSession.ip,
                    "ua": itSession.userAgent,
                    "ui": itSession.id,
                    "ex": (this.sso.expirationTime * 1000) + Date.now()
                };

                this.emit("log", "<=== User (write) cookieValue - " + JSON.stringify(data));
                var cipher = crypto.createCipheriv('des-cbc', privateKey.substr(0, 8), iv);

                var cookieValue = cipher.update(JSON.stringify(data), 'utf8', 'base64');
                cookieValue += cipher.final('base64');


                res.cookie(this.sso.cookieName, cookieValue, {
                    maxAge: (this.sso.expirationTime * 1000),
                    domain: this.sso.adminDomain
                });
                cb(true);
            } else {
                itSession.deleteMe = true;
                cb(false);
            }
        } else {
            cb(true);
        }
    } catch (e) {
        console.log('Exception in addSso - ' + e.message);
    }
};
/**
 * Checks the currently registered session parameters against the incoming request
 * @param itSession
 * @param req
 * @returns {boolean}
 */
SessionMgr.prototype.validateSessionId = function (itSession, req) {
    try {
        if (req.headers['user-agent'] !== itSession.userAgent) {
            throw _root.local.errors.sessionInvalid;
        }
        if (requestIp.getClientIp(req) !== itSession.ip) {
            throw _root.local.errors.sessionInvalid;
        }
        return true;
    } catch (e) {
        return false;
    }
};

SessionMgr.prototype.setLoginFunction = function (func) {
    this.loginFunc = func;
};

SessionMgr.prototype.setSsoFunction = function (func) {
    this.SsoFunc = func;
};

SessionMgr.prototype.getSession = function (sessionId) {
    return getSessionFromGlobalStorage(sessionId);
};

SessionMgr.prototype.updateUsersPermissions = function () {
    for (var key in global.sessions) {
        if (global.sessions.hasOwnProperty(key) && global.sessions[key].id) {
            global.sessions[key].setUserPermissions();
        }
    }
};

SessionMgr.prototype.sendMessageToAllUsers = function (msgObj) {
    var msgKey = msgObj.key; // make sure that the key exists in configuration file
    var msgParams = msgObj.params;
    for (var sessionId in global.sessions) {
        if (global.sessions.hasOwnProperty(sessionId) && global.sessions[sessionId].id) {
            this.emit(msgKey, sessionId, ['login'], msgParams);
        }
    }
};

SessionMgr.prototype.notifyAdminDowntime = function (data) {
    var _root = this;
    this.shutdownAt = Date.now() + (data.shutdownIn * 60000);
    this.shutdownMessage = data.message;
    // send downtime notification to all logged in users 
    this.sendMessageToAllUsers({
        key: 'notifyAdminDowntime',
        params: {
            message: data.message, shutdownIn: data.shutdownIn
        }
    });

    if (this.shutdownInReminder) {
        clearTimeout(this.shutdownInReminder);
    }


    // send downtime notification to all logged in users one min before downtime
    this.shutdownInReminder = setTimeout(function () {
        _root.sendMessageToAllUsers({
            key: 'notifyAdminDowntime',
            params: {
                message: data.message, shutdownIn: 1
            }
        });
    }, ((data.shutdownIn - 1) * 60000));
};

module.exports = {
    SessionMgr: SessionMgr
};
"use strict";
const fs = require('fs');
const moment = require('moment');

/**
 * The log level for outputting
 * @type {string}
 */
var mainLogLevel = 'debug';
/**
 * The absolute path to the log file to be written
 */
var defaultPath = require('os').tmpdir();
/**
 * Loogers cache
 * @type {{}}
 */
var loggers = {};
/**
 * Maximum file size to rotate (default 5 MB)
 * @type {number}
 */
var maxFileSize = 2621440;
/**
 * The main logger object
 * @param name String - the logger name (default is the default)
 * @constructor
 */
function Logger(name) {
    this._name = name || 'default';
    this._logFilePath = defaultPath + "/" + this._name + ".log";
}

// Add all log levels matching method to the prototype
["emergency", "alert", "error", "warn", "info", "debug"].forEach(function (logLevel) {
    Logger.prototype[logLevel] = function () {
        var _root = this, args = arguments;
        setTimeout(function () {
            _root.writer(logLevel, args);
        });
    }
});
/**
 * Writes the log to a file and print to stdout
 * @param logLevel - String
 * @param args - Set
 */
Logger.prototype.writer = function (logLevel, args) {
    var date = new Date().toISOString();
    var exceptions = [];
    var other = [];
    if (args.length > 0) {
        for (let i = 0; i < args.length; i++) {
            if (args[i] instanceof Error) {
                exceptions.push(args[i]);
            } else {
                other.push(args[i]);
            }
        }
    }

    var output = [date, this._name, logLevel.toUpperCase(), other.join(' ')].join(" ");
    if (exceptions.length > 0) {
        for (let i = 0; i < exceptions.length; i++) {
            output += `\n ${exceptions[i].message} \n ${exceptions[i].stack}`;
        }
    }
    switch (logLevel) {
        case "debug":
        case "info":
            console.info(output);
            break;
        case "warn":
            console.warn(output);
            break;
        case "error":
            console.error(output);
            break;
        default:
            console.log(output);
            break;
    }
    fs.appendFile(this._logFilePath, output + "\n", (err) => {
        if (err) throw err;
    });
};

// Rotate log when reaching maxFileSize
setInterval(() => {
    for (var i in loggers) {
        var file = loggers[i]._logFilePath;
        fs.exists(file, (exists) => {
            if (exists) {
                var stats = fs.statSync(file);
                if (stats.size && stats.size > maxFileSize) {
                    var date = moment().format('YYYY-MM-DD_HH:mm:ss');
                    fs.rename(file, file.replace(".log", `_${date}.log`, (err) => {
                        if (err) throw err;
                    }));
                }
            }
        });
    }
}, 300000);

module.exports = {
    /**
     * Set/get the log level (debug is the default level)
     * @param l - String
     */
    set logLevel(l) {
        mainLogLevel = l;
    },
    get logLevel() {
        return mainLogLevel;
    },
    /**
     * Set/get the log file path (os default)
     * @param l - String
     */
    set defaultPath(l) {
        defaultPath = l;
    },
    get defaultPath() {
        return defaultPath;
    },
    /**
     * Get the a logger object
     * @param loggerName - String
     * @returns {*}
     */
    getLogger: function (loggerName) {
        loggerName = loggerName || 'default';
        if (!loggers[loggerName]) {
            loggers[loggerName] = new Logger(loggerName);
        }
        return loggers[loggerName];
    }
};
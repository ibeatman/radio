const util = require('util');
const EventEmitter = require('events');

var numOfTablesCache = 0;
var schemaDB = null;
var allPermissionsData = {};
var permissionsData = {};
/**
 * List of all permission related tables
 * @type {{tables: {userRoles: string, roles: string, rolePermissions: string, permissions: string, users: string}, numOfTables: number}}
 */
const PERMISSIONS_TABLES = {
    'tables': {
        'userRoles': 'user_id',
        'roles': 'id',
        'rolePermissions': 'role_id',
        'permissions': 'id',
        'users': 'id'
    },
    get numOfTables() {
        if (!numOfTablesCache) {
            for (var i in  this.tables) {
                if (this.tables.hasOwnProperty(i))
                    numOfTablesCache++;
            }
        }
        return numOfTablesCache;
    }
};

/**
 * Store all the permission tables and create usersPermissions object that store all
 * permissions per user.
 * Each table that needs to be retrieve should be declared as an empty array
 * @constructor
 */
function PermissionsManager() {
    EventEmitter.call(this);

    Object.defineProperty(this, "allPermissionsData" , {
        get: function () {
            return allPermissionsData;
        }
    });

}
util.inherits(PermissionsManager, EventEmitter);
/**
* Register the database object
* @param db
* @param logger
*/
PermissionsManager.prototype.start = function (schema, logger) {
    schemaDB = schema;
    this.logger = logger.getLogger();
    this.startLoadingData();
};
/**
 * Refresh the data cache
 */
PermissionsManager.prototype.startLoadingData = function () {

    function getAllRows(table) {
        permCounter = 0;
        schemaDB[table].getAllRecords(function (data) {
            permissionsData[table] = {};
            var tableId = PERMISSIONS_TABLES.tables[table];
            for (var i = 0; i < data.length; i++) {
                if (permissionsData[table][data[i][tableId]]) {
                    if (!Array.isArray(permissionsData[table][data[i][tableId]])) {
                        permissionsData[table][data[i][tableId]] = [permissionsData[table][data[i][tableId]]];
                    }
                    permissionsData[table][data[i][tableId]].push(data[i]);
                } else if (tableId != "id") {
                    permissionsData[table][data[i][tableId]] = [data[i]];
                } else {
                    permissionsData[table][data[i][tableId]] = data[i];
                }
            }
            setUsersPermissions(table);
        });
    }

    function setUsersPermissions(table) {
        permCounter++;
        if (permCounter === PERMISSIONS_TABLES.numOfTables) {
            permissionsData.usersPermissions = {};
            for (var userId in permissionsData.users) {
                permissionsData.usersPermissions[userId] = getPermissionsName(getRolesPermissions(getUserRoles(userId)));
            }
            allPermissionsData.usersPermissions = permissionsData.usersPermissions;
            allPermissionsData.users = permissionsData.users;
            allPermissionsData.permissions = permissionsData.permissions;
            allPermissionsData.rolePermissions = permissionsData.rolePermissions;
            allPermissionsData.roles = permissionsData.roles;
            allPermissionsData.userRoles = permissionsData.userRoles;
            allPermissionsData.strictPermissions = [];
            Object.keys(allPermissionsData.permissions).forEach(function (permissionKey) {
                var permission = allPermissionsData.permissions[permissionKey];
                if (permission.is_strict) {
                    allPermissionsData.strictPermissions.push(permission.name);
                }
            });

            _root.emit("loadPermissionsDataEnded");
        }
    }

    function getUserRoles(userId) {
        var userRoles = [];
        try {
            if (Array.isArray(permissionsData.userRoles[userId])) {
                for (var i = 0; i < permissionsData.userRoles[userId].length; i++) {
                    userRoles.push.apply(userRoles, getRoleParents(permissionsData.userRoles[userId][i].role_id));
                }
            } else {
                userRoles.push.apply(userRoles, getRoleParents(permissionsData.userRoles[userId].role_id));
            }
        } catch(e){
            console.log("Failed to getUserRoles - for userId - " + userId, e);
        }
        return userRoles;
    }

    function getRoleParents(role_id) {
        var roleParents = [];
        roleParents.push(role_id);
        if (permissionsData.roles[role_id].parent_id > 0) {
            roleParents.push.apply(roleParents, getRoleParents(permissionsData.roles[role_id].parent_id));
        }
        return roleParents;
    }

    function getRolesPermissions(allUserRoles) {
        var usersPermissions = [];
        allUserRoles = uniq(allUserRoles);
        for (var i = 0; i < allUserRoles.length; i++) {
            if (Array.isArray(permissionsData.rolePermissions[allUserRoles[i]])) {
                for (var ii = 0; ii < permissionsData.rolePermissions[allUserRoles[i]].length; ii++) {
                    usersPermissions.push(permissionsData.rolePermissions[allUserRoles[i]][ii].permission_id);
                }
            } else if (permissionsData.rolePermissions[allUserRoles[i]]) {
                usersPermissions.push(permissionsData.rolePermissions[allUserRoles[i]].permission_id);
            }
        }
        return uniq(usersPermissions);
    }

    function uniq(arr) {
        return Array.from(new Set(arr));
    }

    function getPermissionsName(userRolesPermissions) {
        var permissionsName = [];
        for (var i = 0; i < userRolesPermissions.length; i++) {
            if (permissionsName.indexOf(permissionsData.permissions[userRolesPermissions[i]].name) === -1) {
                permissionsName.push(permissionsData.permissions[userRolesPermissions[i]].name);
            }
        }
        return permissionsName;
    }

    var _root = this;
    var permCounter = 0;

    try {
        for (var table in PERMISSIONS_TABLES.tables) {
            delete this[table];
        }
        for (var table in PERMISSIONS_TABLES.tables) {
            getAllRows(table);
        }
    } catch (e) {
        this.logger.error("Failed to load permissions data", e);
    }
};
/**
 * Save all permissions model changes received from the app
 * @param changes
 * @param callback
 * @param failCallBack
 */
PermissionsManager.prototype.applyChanges = function (changes, callback, failCallBack) {

    function applyChange(changes, i, connection, callback) {
        try {
            if (i === changes.length) {
                callback(true);
            } else {
                var change = changes[i];
                switch (change.action) {
                    case 'addRemovePermission':
                        var add = change.add;
                        var roleId = change.roleId;
                        var permissionId = change.permissionId;
                        if (add) {
                            schemaDB.rolePermissions.insert({
                                data: {
                                    role_id: roleId,
                                    permission_id: permissionId
                                },
                                connection: connection
                            }, function (results) {
                                if (results.rows && results.rows.affectedRows == 1) {
                                    applyChange(changes, ++i, connection, callback);
                                } else {
                                    // TODO : throw is not catched
                                    throw new Error(`Unable to add role permission roleId ${roleId} & permissionId ${permissionId}`);
                                }
                            });
                        } else {
                            schemaDB.rolePermissions.removeComplex({
                                conditions: [
                                    ['role_id', '=', roleId],
                                    'AND',
                                    ['permission_id', '=', permissionId]
                                ],
                                connection: connection
                            }, function (results) {
                                if (results.rows && results.rows.affectedRows == 1) {
                                    applyChange(changes, ++i, connection, callback);
                                } else {
                                    throw new Error(`Unable to remove role permission roleId ${roleId} & permissionId ${permissionId}`);
                                }
                            });
                        }
                        break;
                    case 'addRemoveUserRole':
                        var add = change.add;
                        var roleId = change.roleId;
                        var userId = change.userId;
                        if (add) {
                            schemaDB.userRoles.insert({
                                data: {
                                    role_id: roleId,
                                    user_id: userId
                                },
                                connection: connection
                            }, function (results) {
                                if (results.rows.affectedRows == 1) {
                                    applyChange(changes, ++i, connection, callback);
                                } else {
                                    throw new Error(`Unable to add userRole roleId ${roleId} & userId ${userId}`);
                                }
                            });
                        } else {
                            schemaDB.userRoles.removeComplex({
                                conditions: [
                                    ['role_id', '=', roleId],
                                    'AND',
                                    ['user_id', '=', userId]
                                ],
                                connection: connection
                            }, function (results) {
                                if (results.rows.affectedRows == 1) {
                                    applyChange(changes, ++i, connection, callback);
                                } else {
                                    throw new Error(`Unable to remove userRole roleId ${roleId} & userId ${userId}`);
                                }
                            });
                        }
                        break;

                    case 'addRemoveParentRole':
                        var add = change.add;
                        var roleId = change.roleId;
                        var parentId = change.add ? change.parentId : 0;
                        schemaDB.roles.updateByCondition({
                            conditions: [
                                ['id', '=', roleId]],
                            updates: [{
                                parent_id: parentId
                            }],
                            connection: connection
                        }, function (results) {
                            if (results.rows.affectedRows == 1) {
                                applyChange(changes, ++i, connection, callback);
                            } else {
                                throw new Error(`Unable to ${add ? 'add' : 'remove'} parent role - roleId ${roleId} & parentId ${parentId}`);
                            }
                        });
                        break;

                    case 'setUserFirstView':
                        var userId = change.userId;
                        var firstView = change.firstView;
                        schemaDB.users.updateByCondition({
                            conditions: [
                                ['id', '=', userId]],
                            updates: [{
                                first_view2: firstView
                            }],
                        }, function (results) {
                            if (results.rows.affectedRows == 1) {
                                applyChange(changes, ++i, connection, callback);
                            } else {
                                throw new Error(`Unable to set user first view - userId ${userId} & firstView ${firstView}`);
                            }
                        });
                        break;

                    case 'setRoleFirstView':
                        var roleId = change.roleId;
                        var firstView = change.firstView;
                        schemaDB.roles.updateByCondition({
                            conditions: [
                                ['id', '=', roleId]],
                            updates: [{
                                first_view2: firstView
                            }],
                        }, function (results) {
                            if (results.rows.affectedRows == 1) {
                                applyChange(changes, ++i, connection, callback);
                            } else {
                                throw new Error(`Unable to set role first view - roleId ${roleId} & firstView ${firstView}`);
                            }
                        });
                        break;

                    default:
                        throw new Error(`Unknown action ${change.action}`);
                        break;
                }
            }
        } catch (Ex) {
            callback(false, Ex.message);
        }
    }

    var _root = this;

    schemaDB.startTransaction(function (connection) {
        applyChange(changes, 0, connection, function (status, message) {
            if (status) {
                schemaDB.commitTransaction(connection, function (err) {
                    if (err) {
                        failCallBack(false, err);
                    } else {
                        callback(status);
                        _root.emit("applyChanges");
                    }
                });
            } else {
                schemaDB.rollbackTransaction(connection, function () {
                    failCallBack(false, message);
                });
            }
        });
    });
};


module.exports = PermissionsManager;


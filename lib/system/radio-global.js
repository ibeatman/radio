"use strict";

var
    /**
     * Reference to the app.js module
     * @type {Object}
     */
    expressAppContext = null,
    /**
     * The application context object
     * @type {null}
     */
    radioContext = null,
    /**
     * Express routers object
     * @type {null}
     */
    routes = null,
    /**
     * Absolute path to the app main folder
     * @type {null}
     */
    globalAppRoot = null,
    /**
     * Language code for the localization strings
     * @type {null}
     */
    language = null;


/**
 * Replacing configuration with current environment config, if different from development.
 * @param config
 * @returns {*} altered config
 */
function envConfiguration(config) {
    var envConfigName = process.env.ENV_CONFIG;
    if (!envConfigName) {
        return config;
    }
    var envConfig = require(globalAppRoot + '/etc/configuration/' + envConfigName);
    return Object.assign(config, envConfig);
}

function initRadio() {

    var actionsCache = {};
    var config = require(globalAppRoot + '/etc/configuration/development');
    config = envConfiguration(config);
    // language = config.language || "en";
    // var itDatabase = expressAppContext.get('env') === 'development' ? require(globalAppRoot + '/../libs/it-database') : require("it-database");
    // var itPresto = expressAppContext.get('env') === 'development' ? require(globalAppRoot + '/../libs/it-presto') : require("it-presto");
    var logger = require(globalAppRoot + '/lib/system/it-log');

    logger.logLevel = config.logger.logLevel;
    logger.defaultPath = globalAppRoot + config.logger.defaultPath;

    // var localStrings = require(globalAppRoot + '/etc/local.json')[language] || {};
    var permissionsMgr = new (require(globalAppRoot + '/lib/system/permissions'));
    var siteMap = new (require(globalAppRoot + '/lib/system/site-map'));

    var obj = {
        config: config,
        exports: {
            sessionMgr: require(globalAppRoot + '/lib/system/it-auth').sessionMgr(config.session, permissionsMgr),
            siteMap: siteMap,
            actionsCache: function (action) {
                if (!actionsCache[action]) {
                    try {
                        actionsCache[action] = require(globalAppRoot + "/lib/actions")(action);
                    } catch (e) {
                        logger.getLogger().error("Faild to require a new module to actions cache", e);
                    }
                }
                return actionsCache[action];
            },
            logger: logger,
            permissionsMgr: permissionsMgr
        }
    };

    // require(globalAppRoot + '/lib/system/server-alerts')(obj.exports.itSessionMgr, config.messages);


    // obj.exports.emailGenerator = expressAppContext.get('env') === 'development' ? require(globalAppRoot + '/../libs/it-email-generator') : require("it-email-generator");
    // obj.exports.emailGenerator = obj.exports.emailGenerator({
    //     smtpConfig: config.mail.smtpConfig,
    //     templatesDir: config.mail.templatesDir,
    //     appRoot: globalAppRoot,
    //     logger: logger
    // });


    // require(globalAppRoot + '/lib/system/authentication')(obj.exports.db, obj.exports.itSessionMgr);

    // The second call to buildERD is calling the permissionsMgr.start when finished 
    // obj.exports.db.schema.buildERD(function () {
    //     // Register the database object with permissionMgr
    //     permissionsMgr.start(obj.exports.db, logger);
    // });

    /*
     // reload permissions every rebuild schema
        setInterval(
            function () {
                obj.exports.db.schema.buildERD(function () {
                    logger.getLogger().info('Building permissions..');
                    // Register the database object with permissionMgr
                    permissionsMgr.start(obj.exports.db, logger);
                });
            }, config.database.rebuildSchemaInterval || 86400000);
    */

    // // Changes to the permissions tables took place
    // permissionsMgr.on("applyChanges", function () {
    //     logger.getLogger().info('after applyChanges');
    //     permissionsMgr.startLoadingData();
    // });

    // // New permissions data cache was loaded into the session manager
    // permissionsMgr.on("loadPermissionsDataEnded", function () {
    //     logger.getLogger().info('loadPermissionsDataEnded');
    //     obj.exports.itSessionMgr.updateUsersPermissions();
    // });

    obj.exports.sessionMgr.on("log", function (message) {
        logger.getLogger().info(message);
    });

    return obj;
}

function AppContext() {
}

module.exports = {
    set expressAppContext(appContext) {
        expressAppContext = appContext;
        radioContext = initRadio();
        AppContext.prototype = radioContext.exports;
    },
    get appContext() {
        return radioContext.exports;
    },
    get appContextProto() {
        return AppContext;
    },
    get routes() {
        if (!routes) {
            routes = {
                index: require(`${globalAppRoot}/routes/index`)
                // login: require(`${globalAppRoot}/routes/routes_login`),
                // main: require(`${globalAppRoot}/routes/routes_main`),
                // api: require(`${globalAppRoot}/routes/routes_api`),
                // users: require(`${globalAppRoot}/routes/users`)
            };
        }
        return routes;
    },
    set appRoot(path) {
        globalAppRoot = path;
    },
    get appRoot() {
        return globalAppRoot;
    }
};
"use strict";


//TODO: Refactor to object based
var messagesParams = null;
var itSessionMgr = null;

/**
 * send a message to client with the relevant message data that in the configuration messages
 * @param eventName
 * @param sessionId
 * @param permissions
 * @param joinedData
 */
function sendMessageToClient(eventName, sessionId, permissions, joinedData) {
    var session = itSessionMgr.getSession(sessionId);
    if (session && session.hasPermissions(permissions) && session.webSocketConn) {
        session.webSocketConn.forEach(function (ws) {
            ws.send(JSON.stringify({
                name: eventName,
                data: joinedData
            }), function (err) {
            });
        });
    }
}

/**
 * create listeners for each message in the configuration messages
 */
function createEventslisteners() {
    for (let eventName in messagesParams) {
        if (messagesParams.hasOwnProperty(eventName)) {
            itSessionMgr.on(eventName, (sessionId, permissions, data)=> {
                data = data || {};
                var joinedData = messagesParams[eventName] || {};
                for (let key in data) {
                    if (data.hasOwnProperty(key)) {
                        joinedData[key] = data[key];
                    }
                }
                sendMessageToClient(eventName, sessionId, permissions, joinedData);
            });
        }
    }
}

module.exports = function (itSesMgr, confMessagesParams) {
    messagesParams = confMessagesParams;
    itSessionMgr = itSesMgr;
    createEventslisteners();
};
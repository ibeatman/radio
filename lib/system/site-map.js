"use strict";
const fs = require('fs');

const googleSiteMapeFile = __dirname + '/../../public/sitemap.txt';

function SiteMapManager() {
    fs.exists(googleSiteMapeFile, (exists) => {
        if (exists) {
            fs.unlink(googleSiteMapeFile, function (err) {
                if (err) throw err;
                createIcons();
            });
        } else {
            createIcons();
        }
    });

}


var radioCategories = {};
var stations = {};

function addPageToSiteMap(station, isSecure) {
    var page = 'http' + (isSecure ? 's' : '') + '://www.radiofm.co.il' + (station ? "/" + station : '');
    fs.appendFile(googleSiteMapeFile, page + "\n", (err) => {
        if (err) throw err;
    });
};

function createIcons() {
    addPageToSiteMap(undefined,true);
    var siteMap = require('../../etc/configuration/radio-sitemap');
    var _root = this;
    var _radioCategories = {};
    var _stations = {};
    function Icon(name, type) {
        var name = name;
        var type = type;
    }

    for (var i = 0; i < siteMap.stationsCategories.length; i++) {
        var category = siteMap.stationsCategories[i];
        _radioCategories[category.name] = {
            name: category.name,
            icon: new Icon(category.name, "category"),
            stations: {}
        }
        for (var station in siteMap.stationsCategories[i].stations) {
            var isSecure = siteMap.stationsCategories[i].stations[station].stationURL.indexOf('https') == 0 ? true : false;
            addPageToSiteMap(station, isSecure);
            _radioCategories[category.name].stations[station] = {
                id: station,
                icon: new Icon(station, "station"),
                secure: isSecure,
                name: siteMap.stationsCategories[i].stations[station].name,
                description: siteMap.stationsCategories[i].stations[station].description,
                linkText: siteMap.stationsCategories[i].stations[station].linkText
            }, _stations[station] = {
                id: station,
                icon: new Icon(station, "station"),
                name: siteMap.stationsCategories[i].stations[station].name,
                description: siteMap.stationsCategories[i].stations[station].description,
                stationURL: siteMap.stationsCategories[i].stations[station].stationURL,
                linkText: siteMap.stationsCategories[i].stations[station].linkText
            }
        }
    };
    radioCategories = _radioCategories;
    stations = _stations;
};


SiteMapManager.prototype.getStationUIObject = function (stationKey) {

    var title = stationKey && stations[stationKey] ? "\u05e8\u05d3\u05d9\u05d5 | \u05e8\u05d3\u05d9\u05d5 " + stations[stationKey].linkText + ' | ' + stations[stationKey].linkText :
        "רדיו אונליין | radiofm.co.il | רדיו";

    var description = "הרדיו של ישראל!!! רדיו להאזנה אונליין, אתרי האזנה למוזיקה אונליין, אתרי מוסיקה מהארץ ומהעולם." + (stations[stationKey] ? stations[stationKey].linkText : "");
    var keywords = "רדיו, רדיו אינטרנטי, רדיו מזרחית, רדיו יוונית, רדיו דתי, רדיו אונליין, רדיו באינטרנט, רדיו ישראלי, רדיו בנייד, תחנות רדיו באינטרנט, תחנות רדיו בנייד, תחנות רדיו אונליין, שירים, אקטואליה, חדשות" + (stationKey && stations[stationKey] ? ", " + stations[stationKey].name : "");

    var stationLinkText = stationKey && stations[stationKey] ? stations[stationKey].linkText : 'רדיו אונליין';
    var stationDescription = "         עכשיו הינך מאזין לstationLinkText, ההאזנה לstationLinkText הינה בחינם והיא אחת מעשרות תחנות רדיו שמשדרות עכשיו." +
        "     יש באפשרותך להאזין גם מהנייד וגם מהמחשב לתחנות רדיו אונליין." +
        "    רדיו אפ אם סי או איי אל (radiofm.co.il) באינטרנט מאפשרת לך להאזין לכל תחנות הרדיו המקוונות העליונות בישראל " +
        "    כרגע מתנגנת ברקע תחנת stationLinkText.";
    stationDescription = stationDescription.replace(/stationLinkText/g, stationLinkText);
    if (stationKey && stations[stationKey] && stations[stationKey].description) {
        stationDescription += stations[stationKey].description;
    }

    return {
        title: title,
        meta: {
            description: description,
            keywords: keywords
        },
        station: {
            description: stationDescription,
            link: stationKey && stations[stationKey] ? stations[stationKey].stationURL : '',
            linkText: stationLinkText
        },
        radioElements: radioCategories
    };
};

module.exports = SiteMapManager;

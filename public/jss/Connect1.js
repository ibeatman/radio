var jugadores = 1;
var primer_jugador = "usuario";
var jugador_actual = primer_jugador;
var color_usuario = 1;
var color_ordenador = (color_usuario == 0) ? 1 : 0; 
var colores = new Array("#bb0000", "#0000bb");
var numero_usuario = 1;
var numero_ordenador = (numero_usuario == 1) ? 2 : 1;
var PicesWidth = 11;
var PicesHight = 7;
var color_huecos = "#002200";                
var tablero_padding = parseInt((PicesWidth + PicesHight) / 10);
var TableWidth = 7;
var TableWidth_maximo = 25; 
var TableHight = 6;
var TableHight_maximo = 25; 
var tablero 
var PicesToWin = 4; // number of pieces to win
var gravedad = true;
var diferencia_posicion_horizontal = false;
var diferencia_posicion_vertical = false;
var campo_seleccionable = false;
var columna_caida_inicial = 0;
var columna_caida = 0;
var celda_ultima = 0;
var ficha_cayendo = false;
var se_ha_tirado = false;
var boolGameOver = false;
var intGameTime = 0;

function init(pices) {
	PicesToWin = 4;//pices;
	CreateTable();
}
function updateTime() {
	var _intGameTime = 0;
	var temp = 0;
	if(!boolGameOver && intGameTime < 99999) {
		intGameTime++;
		_intGameTime = intGameTime;
		for (i=_intGameTime.toString().length - 1;i>=0;i--)
		{
			temp = parseInt(_intGameTime / Math.pow(10,i))
			document.images['t'+i].src = "images/number"+temp+".gif"
			_intGameTime = _intGameTime - temp * Math.pow(10,i)		
		}
		theTimer = window.setTimeout("updateTime();",10);
	}
}
						
function animacion_caida(celda)
{
    var fila_caida = Math.ceil( (celda + 1 ) / TableWidth); 
    columna_caida_inicial = (celda - ((fila_caida - 1) * TableWidth)); 
    columna_caida = (celda - ((fila_caida - 1) * TableWidth)); 
    celda_ultima = calcular_caida(celda); 
    ficha_cayendo = setInterval('document.getElementById(columna_caida).src = (jugador_actual == "usuario") ? "images/enterblue.jpg": "images/enterred.jpg"; if (columna_caida > columna_caida_inicial) { document.getElementById(eval(columna_caida-TableWidth)).src = "images/notentered.jpg";} columna_caida += TableWidth; if (columna_caida >= celda_ultima) { clearInterval(ficha_cayendo); ficha_cayendo = false; document.getElementById(eval(columna_caida-TableWidth)).src = "images/notentered.jpg";	poner_pieza_2('+celda+');}', 25);
}

function ShowBar(possition)
{
    for (x=0; x<TableWidth && gravedad; x++)
    {
    	if(x==possition && !boolGameOver){
    		document.getElementById(x+'_show').src = "images/blue.jpg"
    	}
    	else{
    		document.getElementById(x+'_show').src = "images/blank.jpg"
    	}
    }
}

function CreateTable()
{
		boolGameOver = false;
    tablero_padding = parseInt((PicesWidth + PicesHight) / 10);
    ficha_cayendo = false;
    tablero = new Array(TableWidth * TableHight);
		jugador_actual = "usuario";
    intGameTime = 0
    
    var numero_columna = 0; 
    var numero_fila = 0; 
    var hueco_left = 0; 
    var hueco_top = 0; 
    var codigo_html = ""; 
    if (gravedad){
	    for (x=0; x<TableWidth ; x++)
	    {
	    	codigo_html += '<img src=\"images/blank.jpg\" id="'+x+'_show" onmouseover="ShowBar('+x+')" onmouseout="ShowBar(-1)">';
	    }
	    codigo_html += '<br>';
	  }
    for (x=0; x<tablero.length; x++)
    {
        tablero[x] = 0;
        hueco_left = numero_columna * (PicesWidth + tablero_padding);
        hueco_top = numero_fila * (PicesHight + tablero_padding);
        codigo_html += '<img src=\"images/notentered.jpg	\" id="'+x+'" onmouseover="ShowBar('+numero_columna+')" onmouseout="ShowBar(-1)" onClick="if (jugador_actual == \'usuario\' && !ficha_cayendo) { poner_pieza('+x+'); }">';
        numero_columna++;
        if (numero_columna >= TableWidth) { numero_columna = 0; numero_fila++; codigo_html += "<br>";}
    }
    
		document.all['MainBoard'].innerHTML = codigo_html;
		for(i=0;i<=4;i++){
			document.images['t'+i].src = "images/number0.gif"
		}
//		document.getElementById("submit").style.display = "none"
//		document.getElementById("tryagain").style.display = "none"
}


function iniciar_juego_primera_vez()
{
    var primer_jugador_seleccionado = 0;
    if (primer_jugador != "usuario") {
    	primer_jugador_seleccionado = 1;
    }

    document.getElementById("zona_juego").style.visibility = "visible";

    setTimeout('iniciar_juego();', 10);
}


function iniciar_juego()
{
    intGameTime = 0;
    boolGameOver = false;
		for(i=0;i<=4;i++){
			document.images['t'+i].src = "images/number0.gif"
		}
    jugador_actual = primer_jugador;
    setTimeout('CreateTable(); tirar(); updateTime();', 10);
}

function elegir_mejor_hueco()
{
    if (jugador_actual == "usuario") { return -1; }
    
    var mejor_hueco = -1;

    mejor_hueco = buscar_mejor_hueco("ordenador", PicesToWin, false);
    if (mejor_hueco < 0) { mejor_hueco = buscar_mejor_hueco("usuario", PicesToWin, true); }
    if (mejor_hueco < 0 && PicesToWin-1 > 1) { if (TableWidth == 3 && TableHight == 3 && !gravedad && PicesToWin == 3 && tablero[4] == 0 && parseInt(Math.random() * 2) == 1) { return 4; } mejor_hueco = buscar_mejor_hueco("usuario", PicesToWin-1, true); }
    if (!gravedad && mejor_hueco < 0 && PicesToWin-2 > 1) { mejor_hueco = buscar_mejor_hueco("usuario", PicesToWin-2, true); }
    if (mejor_hueco < 0 && PicesToWin-1 > 1) { for (fichas_bucle=PicesToWin-1; fichas_bucle>1; fichas_bucle--) { mejor_hueco = buscar_mejor_hueco("ordenador", fichas_bucle, true); if (mejor_hueco >= 0) { break; } } }
    if (mejor_hueco < 0 && PicesToWin-2 > 1) { for (fichas_bucle=PicesToWin-2; fichas_bucle>1; fichas_bucle--) { mejor_hueco = buscar_mejor_hueco("usuario", fichas_bucle, true); if (mejor_hueco >= 0) { break; } } }
    if (mejor_hueco < 0)
    {
        if (TableWidth == 3 && TableHight == 3 && !gravedad && PicesToWin == 3 && tablero[4] == 0) { return 4; }
        else { while (tablero[mejor_hueco] != 0) { mejor_hueco = parseInt(Math.random() * (tablero.length)); } }
    }
    return mejor_hueco;
}


function buscar_mejor_hueco(jugador, fichas_buscadas, recursivear)
{
    var hueco_buscado = hueco_buscado_horizontal = hueco_buscado_vertical = hueco_buscado_diagonal_izquierda = hueco_buscado_diagonal_derecha = -1;
    
    var numero_jugador = (jugador == "usuario") ? numero_usuario : numero_ordenador;
   
    var bucle_tope = 0;
    if (gravedad) { bucle_tope = TableWidth; }
    else { bucle_tope = tablero.length; }
    
    if (bucle_tope)
    {
        var celda_al_caer = -1;
        for (celda_contador=0; celda_contador < bucle_tope; celda_contador++)
        {
            if (gravedad) { celda_al_caer = calcular_caida(celda_contador); var fila = Math.ceil( (celda_al_caer + 1 ) / TableWidth); var columna = celda_contador; }
            else { celda_al_caer = celda_contador; var fila = Math.ceil( (celda_al_caer + 1 ) / TableWidth); var columna = (celda_al_caer - ((fila - 1) * TableWidth)); }
            if (gravedad && celda_al_caer < 0) { continue; }
            if (!gravedad && tablero[celda_al_caer] != 0) { continue; }

            var tablero_ficticio = new Array(TableWidth*TableHight);
            tablero_ficticio = tablero.slice(0, tablero.length);
            tablero_ficticio[celda_al_caer] = numero_jugador;

            if (recursivear)
            {
                var contador_backup = celda_contador;
                if (buscar_mejor_hueco("usuario", PicesToWin, false) < 0)
                {
                    celda_contador = contador_backup;
                    tablero[celda_al_caer] = 2;
                    if (buscar_mejor_hueco("usuario", PicesToWin, false) >= 0)
                    {
                        tablero[celda_al_caer] = 0;
                        celda_contador = contador_backup;
                        if (gravedad && celda_al_caer+1 >= TableWidth || !gravedad && celda_al_caer+1 >= tablero.length) { return -1; }
                        else { continue; }
                    }
                    else { celda_contador = contador_backup; tablero[celda_al_caer] = 0; } //Quita la ficha ficticia.
                }
            }

            var primera_celda_fila = (fila - 1) * TableWidth; 
            var primera_celda_columna = columna - 1; 
            var primera_celda_diagonal_izquierda_superior = celda_al_caer - (columna - 1) - ((columna - 1) * TableWidth) - TableWidth - 1;
            if (primera_celda_diagonal_izquierda_superior < 0) { primera_celda_diagonal_izquierda_superior = Math.abs(primera_celda_diagonal_izquierda_superior / TableWidth); }
            var primera_celda_diagonal_derecha_superior = celda_al_caer + (TableWidth - columna) - ((TableWidth - columna) * TableWidth) + TableWidth - 1;
            if (primera_celda_diagonal_derecha_superior < 0) { primera_celda_diagonal_derecha_superior = Math.ceil(TableWidth - 1 - Math.abs(primera_celda_diagonal_derecha_superior / TableWidth)) - 1; }
            
            var fichas_seguidas = 0; 
            var x_fila = Math.ceil( (primera_celda_diagonal_izquierda_superior + 1) / TableWidth); //Numero de fila donde se encuentra la celda elegida.
            var x_columna = (primera_celda_diagonal_izquierda_superior - ((x_fila - 1) * TableWidth)) + 1; //Numero de columna donde se encuentra la celda elegida.
            for (x = primera_celda_diagonal_izquierda_superior; x <= TableWidth*TableHight; x+=TableWidth+1)
            {
                if (tablero_ficticio[x] == numero_jugador) { fichas_seguidas++; } //Si el hueco actual tiene una ficha del jugador actual, se cuenta como seguida.
                else { fichas_seguidas = 0; } //...pero si no, se pone a cero.
                if (fichas_seguidas >= fichas_buscadas)
                {
                    hueco_buscado_diagonal_izquierda = celda_al_caer;
                    if (fila-PicesToWin+fichas_seguidas-1 < 0 || columna+PicesToWin-fichas_seguidas+1 > TableWidth || columna-PicesToWin+fichas_seguidas < 0) { hueco_buscado_diagonal_izquierda = -1; fichas_seguidas = 0; }
                }
                if (x_columna >= TableWidth || x_fila >= TableHight) { break; }
                x_fila++;
                x_columna++;
            }
            if (hueco_buscado_diagonal_izquierda > 0) { break; }

            fichas_seguidas = 0;
            var x_fila = Math.ceil( (primera_celda_diagonal_derecha_superior + 1 ) / TableWidth); //Numero de fila donde se encuentra la celda elegida.
            var x_columna = (primera_celda_diagonal_derecha_superior - ((x_fila - 1) * TableWidth)) + 1; //Numero de columna donde se encuentra la celda elegida.
            for (x = primera_celda_diagonal_derecha_superior; x <= TableWidth*TableHight; x+=TableWidth-1)
            {
                if (tablero_ficticio[x] == numero_jugador) { fichas_seguidas++; } //Si el hueco actual tiene una ficha del jugador actual, se cuenta como seguida.
                else { fichas_seguidas = 0; } //...pero si no, se pone a cero.
                if (fichas_seguidas >= fichas_buscadas)
                {
                    hueco_buscado_diagonal_derecha = celda_al_caer;
                    if (fila-PicesToWin+fichas_seguidas-1 < 0 || columna+PicesToWin-fichas_seguidas+1 > TableWidth || columna-PicesToWin+fichas_seguidas < 0) { hueco_buscado_diagonal_derecha = -1; fichas_seguidas = 0; }
                }
                if (x_columna <= 1 || x_fila >= TableHight) { break; }
                x_fila++;
                x_columna--;
            }
            if (hueco_buscado_diagonal_derecha > 0) { break; }
            
            fichas_seguidas = 0;
            for (x = primera_celda_fila; x <= primera_celda_fila+TableWidth-1; x++)
            {
                if (tablero_ficticio[x] == numero_jugador) { fichas_seguidas++; } //Si el hueco actual tiene una ficha del jugador actual, se cuenta como seguida.
                else { fichas_seguidas = 0; } 
                if (fichas_seguidas >= fichas_buscadas)
                {
                    hueco_buscado_horizontal = celda_al_caer;
                    if (columna+PicesToWin-fichas_seguidas+1 > TableWidth || columna-PicesToWin+fichas_seguidas < 0) { hueco_buscado_horizontal = -1; fichas_seguidas = 0; }
                }
            }
            if (hueco_buscado_horizontal > 0 && gravedad) { break; }

            fichas_seguidas = 0;
            if (gravedad) { var principio_bucle = celda_al_caer; var final_bucle = celda_al_caer+(TableWidth*fichas_buscadas); }
            else { var principio_bucle = columna; var final_bucle = tablero.length - TableWidth + columna; }
            for (x = principio_bucle; x <= final_bucle; x += TableWidth)
            {
                if (tablero_ficticio[x] == numero_jugador) { fichas_seguidas++; } //Si el hueco actual tiene una ficha del jugador actual, se cuenta como seguida.
                else { fichas_seguidas = 0; } 
                if (fichas_seguidas >= fichas_buscadas)
                {
                    hueco_buscado_vertical = celda_al_caer;
                    if (fila-PicesToWin+fichas_seguidas-1 < 0) { hueco_buscado_vertical = -1; fichas_seguidas = 0; }
                }
            }
            if (hueco_buscado_vertical > 0) { break; }
        }
    }

    if (hueco_buscado_horizontal < 0 && hueco_buscado_vertical < 0 && hueco_buscado_diagonal_izquierda < 0 && hueco_buscado_diagonal_derecha < 0) { return -1; }
    
    var huecos = new Array(hueco_buscado_horizontal, hueco_buscado_vertical, hueco_buscado_diagonal_izquierda, hueco_buscado_diagonal_derecha);
    while (hueco_buscado < 0) { hueco_buscado = huecos[parseInt(Math.random() * (huecos.length))]; }
    
    return hueco_buscado;
}


function calcular_caida(celda)
{
    var fila = Math.ceil( (celda + 1 ) / TableWidth); //Numero de fila donde se encuentra la celda elegida.
    var columna = (celda - ((fila - 1) * TableWidth)) + 1; //Numero de columna donde se encuentra la celda elegida.
    for (x=columna-1; x<=TableWidth*TableHight-1; x+=TableWidth)
    {
        if (tablero[x] != 0) { break; }
    }
    if (x-TableWidth < 0) { return -1; }
    else { return x-TableWidth; }
}

function tirar()
{
    setTimeout("var mejor_hueco = elegir_mejor_hueco(); poner_pieza(mejor_hueco);", 10);
}                


function poner_pieza(celda)
{
    if (intGameTime == 0) { updateTime(); }
    if (jugador_actual == "usuario" && celda < 0) { return; }
    if (tablero[celda] != 0) { return; }
    
    se_ha_tirado = true;
    
    if (gravedad)
    {
        celda = calcular_caida(celda);
        
        animacion_caida(celda);
    } else { poner_pieza_2(celda); } 
}


function poner_pieza_2(celda)
{
    tablero[celda] = (jugador_actual == "usuario") ? numero_usuario : numero_ordenador;
    
    document.getElementById(celda).src = (jugador_actual == "usuario") ? "images/enterblue.jpg" : "images/enterred.jpg";
    
    document.getElementById(celda).style.cursor = "default";
   
    partida_acabada = comprobar_ganador(celda);
    
    if (partida_acabada) { return; }

    if (jugadores == 1) 
    { 
    	jugador_actual = (jugador_actual == "usuario") ? "ordenador" : "usuario"; 
    }
    else 
  	{ 
  		jugador_actual = "usuario"; 
  		var color_usuario_backup = colores[color_usuario]; 
  		colores[color_usuario] = colores[color_ordenador]; 
  		colores[color_ordenador] = color_usuario_backup; 
  		document.getElementById("pieza").src = "images/notentered.jpg"; 
  		numero_usuario = (numero_usuario == 1) ? 2 : 1; 
  	}

    tirar();
}


function comprobar_ganador(celda)
{
    var ganador = "";
    
    var tablero_lineas = new Array(TableWidth*TableHight);
    for (x_tablero_lineas = 0; x_tablero_lineas < tablero_lineas.length; x_tablero_lineas++) 
    { 
    	tablero_lineas[x_tablero_lineas] = 0; 
    }
    
    var numero_jugador;
    if (jugador_actual == "usuario"){
    	numero_jugador = numero_usuario ;
    }
    else{
    	numero_jugador =  numero_ordenador;
    }
    var fila = Math.ceil( (celda + 1 ) / TableWidth); //Numero de fila donde se encuentra la celda elegida.
    var columna = (celda - ((fila - 1) * TableWidth)) + 1; //Numero de columna donde se encuentra la celda elegida.
    var primera_celda_fila = (fila - 1) * TableWidth; //Numero de la primera celda que esta en la misma columna que la celda elegida.
    var primera_celda_columna = columna - 1; //Numero de la primera celda que esta en la misma fila que la celda elegida.
    var primera_celda_diagonal_izquierda_superior = celda - (columna - 1) - ((columna - 1) * TableWidth);
    if (primera_celda_diagonal_izquierda_superior < 0) { primera_celda_diagonal_izquierda_superior = Math.abs(primera_celda_diagonal_izquierda_superior / TableWidth); }
    var primera_celda_diagonal_derecha_superior = celda + (TableWidth - columna) - ((TableWidth - columna) * TableWidth);
    if (primera_celda_diagonal_derecha_superior < 0) { primera_celda_diagonal_derecha_superior = Math.ceil(TableWidth - 1 - Math.abs(primera_celda_diagonal_derecha_superior / TableWidth)) - 1; }
    var fichas_seguidas = 0; //Contador de fichas seguidas, para saber si ha hecho linea.
    
    var comienzo_linea = primera_celda_fila;
    for (x = primera_celda_fila; x <= primera_celda_fila+TableWidth-1; x++)
    {
        if (tablero[x] == numero_jugador) 
        { 
        	fichas_seguidas++; 
        } 
        else 
        { 
        	fichas_seguidas = 0; 
        	comienzo_linea = x;
        } 
        if (fichas_seguidas >= PicesToWin)
        {
            var comienzo_bucle 
            if (comienzo_linea == primera_celda_fila) 
            {
            	comienzo_bucle = comienzo_linea ;
            }
            else
          	{
          		comienzo_bucle = comienzo_linea+1;
          	}
            var salir_del_bucle = false;
            for (contador_comienzo_linea = comienzo_bucle; contador_comienzo_linea <= primera_celda_fila+TableWidth-1; contador_comienzo_linea++) 
            { 
            	if (tablero[contador_comienzo_linea] == numero_jugador) 
            	{ 
            		document.getElementById(contador_comienzo_linea).src = (jugador_actual == "usuario") ? "images/winblue.jpg": "images/winred.jpg";
            		tablero_lineas[contador_comienzo_linea] = 1; 
            		salir_del_bucle = true; 
            	} 
            	else if (salir_del_bucle) { break; }
            }
            ganador = jugador_actual;
            break;
        } 
    }
    comienzo_linea = primera_celda_columna;
    
    fichas_seguidas = 0;
    for (x = primera_celda_columna; x <= TableWidth*TableHight-1; x+=TableWidth)
    {
        if (tablero[x] == numero_jugador) 
        { 
        	fichas_seguidas++; 
        }
        else 
        { 
        	fichas_seguidas = 0; 
        	comienzo_linea = x; 
        } 
        if (fichas_seguidas >= PicesToWin)
        {
            var comienzo_bucle = (comienzo_linea == primera_celda_columna) ? comienzo_linea : comienzo_linea+TableWidth;
            salir_del_bucle = false;
            for (contador_comienzo_linea = comienzo_bucle; contador_comienzo_linea <= TableWidth*TableHight-TableWidth+columna-1; contador_comienzo_linea+=TableWidth) 
            { 
            	if (tablero[contador_comienzo_linea] == numero_jugador) 
            	{ 
            		document.getElementById(contador_comienzo_linea).src = (jugador_actual == "usuario") ? "images/winblue.jpg": "images/winred.jpg";
            		tablero_lineas[contador_comienzo_linea] = 1; 
            		salir_del_bucle = true; 
            	} 
            	else if (salir_del_bucle) { break; } 
            }
            ganador = jugador_actual;
            break;
        } 
    }

    comienzo_linea = primera_celda_diagonal_izquierda_superior;
    fichas_seguidas = 0;
    var x_fila = Math.ceil( (primera_celda_diagonal_izquierda_superior + 1) / TableWidth); 
    var x_columna = (primera_celda_diagonal_izquierda_superior - ((x_fila - 1) * TableWidth)) + 1; 
    for (x = primera_celda_diagonal_izquierda_superior; x <= TableWidth*TableHight; x+=TableWidth+1)
    {
        if (tablero[x] == numero_jugador) { fichas_seguidas++; } 
        else { fichas_seguidas = 0; comienzo_linea = x; } 
        if (fichas_seguidas >= PicesToWin)
        {
            for (j=x; j<=TableWidth*TableHight; j+=TableWidth+1) { if (x_columna >= TableWidth || x_fila >= TableHight) { break; } }
            var comienzo_bucle = (comienzo_linea == primera_celda_diagonal_izquierda_superior) ? comienzo_linea : comienzo_linea+TableWidth+1;
            salir_del_bucle = false;
            for (contador_comienzo_linea = comienzo_bucle; contador_comienzo_linea <= j; contador_comienzo_linea+=TableWidth+1) 
            { 
            	if (tablero[contador_comienzo_linea] == numero_jugador) 
            	{ 
            		document.getElementById(contador_comienzo_linea).src = (jugador_actual == "usuario") ? "images/winblue.jpg": "images/winred.jpg";
            		tablero_lineas[contador_comienzo_linea] = 1; 
            		salir_del_bucle = true; 
            	} 
            	else if (salir_del_bucle) { break; } 
            }
            ganador = jugador_actual;
            break;
        } 
        if (x_columna >= TableWidth || x_fila >= TableHight) { break; }
        x_fila++;
        x_columna++;
    }

    comienzo_linea = primera_celda_diagonal_derecha_superior;
    fichas_seguidas = 0;
    var x_fila = Math.ceil( (primera_celda_diagonal_derecha_superior + 1) / TableWidth); 
    var x_columna = (primera_celda_diagonal_derecha_superior - ((x_fila - 1) * TableWidth)) + 1; 
    for (x = primera_celda_diagonal_derecha_superior; x <= TableWidth*TableHight; x+=TableWidth-1)
    {
        if (tablero[x] == numero_jugador) { fichas_seguidas++; } 
        else { fichas_seguidas = 0; comienzo_linea = x; } 
        if (fichas_seguidas >= PicesToWin)
        {
            for (j=x; j<=TableWidth*TableHight; j+=TableWidth-1) { if (x_columna <= 1 || x_fila >= TableHight) { break; } } 
            var comienzo_bucle = (comienzo_linea == primera_celda_diagonal_derecha_superior) ? comienzo_linea : comienzo_linea+TableWidth-1;
            salir_del_bucle = false;
            for (contador_comienzo_linea = comienzo_bucle; contador_comienzo_linea <= j; contador_comienzo_linea+=TableWidth-1) 
            { 
            	if (tablero[contador_comienzo_linea] == numero_jugador) 
            	{ 
            		document.getElementById(contador_comienzo_linea).src = (jugador_actual == "usuario") ? "images/winblue.jpg": "images/winred.jpg";
            		tablero_lineas[contador_comienzo_linea] = 1; 
            		salir_del_bucle = true; 
            	} 
            	else if (salir_del_bucle) 
            		{ break; } 
            }
            ganador = jugador_actual;
            break;
        } 
        if (x_columna <= 1 || x_fila >= TableHight) { break; }
        x_fila++;
        x_columna--;
    }
    
    if (ganador != "")
    {
        if (ganador == "usuario")
        {        	
            	//alert("you won");
//					document.getElementById("submit").style.display = ""
//					document.getElementById("us").value = intGameTime;
        }
        else{
        	//alert("you lose");
//					document.getElementById("tryagain").style.display = ""
        }
			boolGameOver = true;
        	return true; 
    }
    else
    {
        var huecos_libres = false;
        for (x=0; x<tablero.length; x++) { if (tablero[x] == 0) { huecos_libres = true; } }
        if (!huecos_libres)
        {
            	alert("Game has finished, there are not more free holes");
				boolGameOver = true;
 //				document.getElementById("submit").style.display = ""
//					document.getElementById("us").value = intGameTime;
          	return true;
        }
        else { return false; }
    }
}

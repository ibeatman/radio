var gsize, ghrow, ghcol, gtime, gmoves, gintervalid=-1, gshuffling;
var theTimer=0;

function toggleHelp()
{
  if (butHelp.value == "Hide Help")
  {
    help.style.display = "none";
    butHelp.value = "Show Help";
  }
  else
  {
    help.style.display = "";
    butHelp.value = "Hide Help";
  }  
}

function r(low,hi)
{
  return Math.floor((hi-low)*Math.random()+low); 
}

function r1(hi)
{
  return Math.floor((hi-1)*Math.random()+1); 
}

function r0(hi)
{
  return Math.floor((hi)*Math.random()); 
}

function init()
{
	loadBoard(4);
  shuffle();
  gtime = 0;
  gmoves = 0;
//  tickTime();
//  gintervalid = setInterval("tickTime()",1000);
//	document.getElementById("submit").style.display = "none";
	for(i=0;i<=4;i++){
		document.images['t'+i].src = "Images/number0.gif"
	}
	gintervalid=1;
	updateMineTime();
}

function updateMineTime() {
	var _gt = 0;
	var temp = 0;
	if(	gintervalid>=1 && gintervalid < 99999) {
		gintervalid++;
		_gt = gintervalid;
		for (i=_gt.toString().length - 1;i>=0;i--)
		{
			temp = parseInt(_gt / Math.pow(10,i))
			document.images['t'+i].src = "Images/number"+temp+".gif"
			_gt = _gt - temp * Math.pow(10,i)		
		}
		theTimer = window.setTimeout("updateMineTime();",500);
	}
	else{
		clearTimeout(theTimer);
	}
}

function stopGame()
{
  gintervalid=-1;
}

function tickTime()
{
  showStatus();
  gtime++;
}

function checkWin()
{
  var i, j, s;
  if (gintervalid==-1) return; //game not started!
  if (!isHole(gsize-1,gsize-1)) return;
//  alert(gintervalid);
  for (i=0;i<gsize;i++)
    for (j=0;j<gsize;j++)
    {
      if (!(i==gsize-1 && j==gsize-1)) //ignore last block (ideally a hole)
      {
       if (getValue(i,j)!=(i*gsize+j+1).toString()) return;
      }
    }
	WinGame();
  stopGame();
//	document.getElementById("submit").style.display = "";
}

function showStatus()
{
  fldStatus.innerHTML = "Time: " + gtime + " secs   Moves: " + gmoves
}

function showTable()
{
  var i, j, s;
  stopGame();
  s = "<table border=0 cellpadding=0 cellspacing=0>";
  for (i=0; i<gsize; i++)
  {
    s = s + "<tr>";    
    for (j=0; j<gsize; j++)
    {
      s = s + "<td id=a_" + i + "_" + j + " onclick='move(this)' class=cell>" + (i*gsize+j+1) + "</td>";
    }
    s = s + "</tr>";        
  }
  s = s + "</table>";
  return s;
}

function getCell(row, col)
{
  return eval("a_" + row + "_" + col);
}

function setValue(row,col,val)
{
  var v = getCell(row, col);
  v.innerHTML = val;
  v.className = "cell";
}

function getValue(row,col)
{
  var v = getCell(row, col);
  return v.innerHTML;
}

function setHole(row,col)
{ 
  var v = getCell(row, col);
  v.innerHTML = "";
  v.className = "hole";
  ghrow = row;
  ghcol = col;
}

function getRow(obj)
{
  var a = obj.id.split("_");
  return a[1];
}

function getCol(obj)
{
  var a = obj.id.split("_");
  return a[2];
}

function isHole(row, col)
{
  return (row==ghrow && col==ghcol) ? true : false;
}

function getHoleInRow(row)
{
  var i;
  return (row==ghrow) ? ghcol : -1;
}

function getHoleInCol(col)
{
  var i;
  return (col==ghcol) ? ghrow : -1;
}

function shiftHoleRow(src,dest,row)
{
  var i;
  src = parseInt(src);
  dest = parseInt(dest);
  if (src < dest)
  {
    for (i=src;i<dest;i++)
    {
      setValue(row,i,getValue(row,i+1));
      setHole(row,i+1);
    }
  }
  if (dest < src)
  {
    for (i=src;i>dest;i--)
    {
      setValue(row,i,getValue(row,i-1));
      setHole(row,i-1);
    }
  }
}

function shiftHoleCol(src,dest,col)
{
  var i;
  src = parseInt(src);
  dest = parseInt(dest);
  if (src < dest)
  {
    for (i=src;i<dest;i++)
    {
      setValue(i,col,getValue(i+1,col));
      setHole(i+1,col);
    }
  }
  if (dest < src)
  {
    for (i=src;i>dest;i--)
    {
      setValue(i,col,getValue(i-1,col));
      setHole(i-1,col);
    }
  }
}

function move(obj)
{
  var r, c, hr, hc;
  r = getRow(obj);
  c = getCol(obj);
  if (isHole(r,c)) return;
  hc = getHoleInRow(r);
  if (hc != -1)
  {
    shiftHoleRow(hc,c,r);
    gmoves++;
    checkWin();
    return;
  }
  hr = getHoleInCol(c);
  if (hr != -1)
  {
    shiftHoleCol(hr,r,c);
    gmoves++;
    checkWin();
    return;
  }
}

function shuffle()
{
  var t,i,j,s,frac;
  gshuffling =  true;
  frac = 100.0/(gsize*(gsize+10));
  s = "% ";
  for (i=0;i<gsize;i++)
  {
    s += "|";
    for (j=0;j<gsize+10;j++)
    {  
      if (j%2==0)
      {
        t = r0(gsize);
        while (t == ghrow) t = r0(gsize); //skip holes
        getCell(t,ghcol).click();
      } 
      else
      {
        t = r0(gsize);
        while (t == ghcol) t = r0(gsize); //skip holes
        getCell(ghrow,t).click();
      }
    }
  }
}

function loadBoard(size)
{
  gsize = size;
  document.all['MainBoard'].innerHTML = showTable(gsize);
  setHole(gsize-1,gsize-1);
}

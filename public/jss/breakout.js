var timerID = null;
var INT = 40;
var loadFLG = 0;
var gameFLG = 0;
var missFLG = 0;
var tim = 0;
var blcol = new Array(5); // block color
var blsta = new Array(40); // block status
var blNO = new Array(40); // block No
var blclr = 0; // clear block
// ball data
var ballX = 0;
var ballY = 0;
var ballDX = 0;
var ballDY = 0;
var tmpRL = 193;
var X = 0;
blcol[0] = "#FFFF00";
blcol[1] = "#FFCF00";
blcol[2] = "#FF7F00";
blcol[3] = "#FF3F00";
blcol[4] = "#FF0000";
blcol[5] = "#000000";
var testing = 0;
var gt = 0;
var bgo = false;

// field size to edit
var BlocksWidth = 32;
var BlocksHeight = 12;
var BlocksGap = 4;
var FieldWidth = 300;
var FieldHeight = 250;
var RacketWidth = 40;
var RacketHeight = 4;
var BoxesFieldHeight = 85;
var BoxesFieldWidth = 292;
var BoxesFieldGapFromTop = 9;
var BoxesFieldGapFromLeft = 8;
var NumOfBoxesInRow = 8;
var NumOfRows = 5;
//do not edit the following values

function updateMineTime_() {
	var _gt = 0;
	var temp = 0;
	if (!bgo && gt < 99999) {
		gt++;
		_gt = gt;
		for (i = _gt.toString().length - 1; i >= 0; i--) {
			temp = parseInt(_gt / Math.pow(10, i))
			document.images['t' + i].src = "Images/number" + temp + ".gif"
			_gt = _gt - temp * Math.pow(10, i)
		}
		theTimer = window.setTimeout("updateMineTime();", 10);
	}
	else {
		clearTimeout(theTimer);
	}
}

function mainF() {
	tim = tim + 1;
	ballX = ballX + ballDX;
	ballY = ballY + ballDY;
	outCHK();
	blkCHK();
	ball.style.posTop = ballY;
	ball.style.posLeft = ballX;
	racket.style.posLeft = tmpRL;
	if (gameFLG == 01) {
		timerID = setTimeout("mainF()", INT);
	}
	else {
		clearTimeout(timerID);
	}
}

function initG() {
	blclr = 0;
	tim = 0;
	starter.style.posTop = -1000;
	starter.style.posLeft = -1000;
	gameFLG = 1;
	loadFLG = 1;
	ballY = AbsPosY + FieldHeight - 25;
	ballX = AbsPosX + FieldWidth / 2 - 3;
	ballDX = -8;
	ballDY = -8;
	tmpRL = AbsPosX + ballX - 17;
	missFLG = 0;
	mainF();
	gt = 1;
	updateMineTime();
}

function MouseMv() {
	if (loadFLG == 1) {
		tmpRL = X - 8;
		if (tmpRL < AbsPosX) { tmpRL = AbsPosX; }
		if (tmpRL > AbsPosX + FieldWidth - RacketWidth) { tmpRL = AbsPosX + FieldWidth - RacketWidth; }
	}
}

function outCHK() {
	var Angel = -16;
	if (ballX < AbsPosX) { ballX = (2 * (AbsPosX)) - ballX; ballDX = -ballDX; }
	if (ballX > AbsPosX + FieldWidth) { ballX = (2 * (AbsPosX + FieldWidth)) - ballX; ballDX = -ballDX; }
	if (ballY < AbsPosY - 2) { ballY = (2 * (AbsPosY - 2)) - ballY; ballDY = -ballDY; }
	if (ballY >= AbsPosY + FieldHeight - 20) {
		if (missFLG == 0 || testing == 1) {
			tmpX = (ballDX / ballDY) * (AbsPosY + FieldHeight - 20 - ballY) + ballX;//20 is the racket gap from buttom 
			if (tmpX >= tmpRL - 12 || testing == 1) {
				if (tmpX <= tmpRL + BlocksWidth || testing == 1) {
					ballY = AbsPosY + FieldHeight - 20;
					ballDY = -ballDY;
					ballX = tmpX;
					ballRD = tmpX - tmpRL;
					for (var PositionOnRacket = -4; PositionOnRacket <= 36; PositionOnRacket = PositionOnRacket + 4) {
						if (ballRD > PositionOnRacket - 4 && ballRD <= PositionOnRacket) {
							ballDX = Angel;
							//						window.status = ballDX;
							break;
						}
						Angel = Angel + 3;
					}
				}
			}
			if (ballDY > 0) { missFLG = 1; }
		} else {
			if (ballY > AbsPosY + 245) { missFLG = 0; gameEnd(); }
		}
	}
}

function blkCHK() {
	tmpY = ballY + 4;
	tmpX = ballX + 4;
	if (tmpY >= (AbsPosY + 9)) {
		if (tmpY <= AbsPosY + 85) {
			if (tmpX >= (AbsPosX + 8)) {
				if (tmpX <= AbsPosX + 292) {
					with (Math) {
						ia = floor((tmpX - (AbsPosX + 8)) / 36);
						ib = floor((tmpY - (AbsPosY + 9)) / 16);
						ic = ib * 8 + ia;
					}
					if (blsta[ic] <= 4) {
						tmpbc = blsta[ic] + 1;
						blsta[ic] = tmpbc;
						chc(ic, tmpbc);
						if (tmpbc == 5) { blclr = blclr + 1; }
						if (blclr >= 40) { gameEnd(); }


						if (ballDX > 0) {// from left to right
							iy = (ballDY / ballDX) * ((AbsPosX + 8) + 36 * ia - tmpX) + tmpY;
							if (iy > (AbsPosY + 9) + 16 * ib + 9) {
								tmpY1 = (AbsPosY + 9) + 16 * ib + 9;
								tmpX1 = (ballDX / ballDY) * ((AbsPosY + 9) + 16 * ib + 9 - tmpY) + tmpX;
								ballX = tmpX1 - 4;
								ballY = tmpY1 - 4;
								ballDY = -ballDY;
							} else {
								if (iy < 44 + 16 * ib) {
									tmpY1 = (AbsPosY + 9) + 16 * ib;
									tmpX1 = (ballDX / ballDY) * ((AbsPosY + 9) + 16 * ib - tmpY) + tmpX;
									ballX = tmpX1 - 4;
									ballY = tmpY1 - 4;
									ballDY = -ballDY;
								} else {
									tmpX1 = (AbsPosX + 8) + 36 * ia;
									tmpY1 = (ballDY / ballDX) * ((AbsPosX + 8) + 36 * ia - tmpX) + tmpY;
									ballX = tmpX1 - 4;
									ballY = tmpY1 - 4;
									ballDX = -ballDX;
								}
							}
						} else {// from right to left
							iy = (ballDY / ballDX) * ((AbsPosX + 8) + 36 * ia + 44 - tmpX) + tmpY;
							if (iy > (AbsPosY + 9) + 16 * ib + 9) {
								tmpY1 = (AbsPosY + 9) + 16 * ib + 9;
								tmpX1 = (ballDX / ballDY) * ((AbsPosY + 9) + 16 * ib + 9 - tmpY) + tmpX;
								ballX = tmpX1 - 4;
								ballY = tmpY1 - 4;
								ballDY = -ballDY;
							} else {
								if (iy < 44 + 16 * ib) {
									tmpY1 = (AbsPosY + 9) + 16 * ib;
									tmpX1 = (ballDX / ballDY) * ((AbsPosY + 9) + 16 * ib - tmpY) + tmpX;
									ballX = tmpX1 - 4;
									ballY = tmpY1 - 4;
									ballDY = -ballDY;
								} else {
									tmpX1 = (AbsPosX + 8) + 36 * ia + 44;
									tmpY1 = (ballDY / ballDX) * ((AbsPosX + 8) + 36 * ia + 44 - tmpX) + tmpY;
									ballX = tmpX1 - 4;
									ballY = tmpY1 - 4;
									ballDX = -ballDX;
								}
							}
						}
					}
				}
			}
		}
	}
}
function blkCHK_() {
	tmpY = ballY + 4;
	tmpX = ballX + 4;
	if (tmpY >= (AbsPosY + BoxesFieldGapFromTop)) {
		if (tmpY <= AbsPosY + FieldHeight) {
			if (tmpX >= (AbsPosX + BoxesFieldGapFromLeft)) {
				if (tmpX <= AbsPosX + FieldWidth) {
					with (Math) {
						ia = floor((tmpX - (AbsPosX + BoxesFieldGapFromLeft)) / (BlocksWidth + BlocksGap));//colomn
						ib = floor((tmpY - (AbsPosY + BoxesFieldGapFromTop)) / (BlocksHeight + BlocksGap));//row
						ic = ib * 8 + ia;
					}
					if (blsta[ic] <= 4) {
						tmpbc = blsta[ic] + 1;
						blsta[ic] = tmpbc;
						chc(ic, tmpbc);
						if (tmpbc == 5) { blclr = blclr + 1; }
						if (blclr >= 40) { gameEnd(); }


						if (ballDX > 0) {// from left to right
							iy = (ballDY / ballDX) * (AbsPosX + BoxesFieldGapFromLeft + ((BlocksWidth + BlocksGap) * ia) - tmpX) + tmpY;
							if (iy > (AbsPosY + BoxesFieldGapFromTop) + (BlocksHeight + BlocksGap) * ib + BlocksHeight) {//came from down
								tmpY1 = (AbsPosY + BoxesFieldGapFromTop) + (BlocksHeight + BlocksGap) * ib + BlocksHeight;
								tmpX1 = (ballDX / ballDY) * ((AbsPosY + BoxesFieldGapFromTop) + (BlocksHeight + BlocksGap) * ib + BlocksHeight - tmpY) + tmpX;
								ballX = tmpX1 - 4;
								ballY = tmpY1 - 4;
								ballDY = -ballDY;
							} else {
								if (iy < BlocksHeight + (BlocksHeight + BlocksGap) * ib) {
									tmpY1 = (AbsPosY + BoxesFieldGapFromTop) + (BlocksHeight + BlocksGap) * ib;
									tmpX1 = (ballDX / ballDY) * ((AbsPosY + BoxesFieldGapFromTop) + (BlocksHeight + BlocksGap) * ib - tmpY) + tmpX;
									ballX = tmpX1 - 4;
									ballY = tmpY1 - 4;
									ballDY = -ballDY;
								} else {
									tmpX1 = (AbsPosX + BoxesFieldGapFromLeft) + (BlocksWidth + BlocksGap) * ia;
									tmpY1 = (ballDY / ballDX) * ((AbsPosX + BoxesFieldGapFromLeft) + (BlocksWidth + BlocksGap) * ia - tmpX) + tmpY;
									ballX = tmpX1 - 4;
									ballY = tmpY1 - 4;
									ballDX = -ballDX;
								}
							}
						} else {// from right to left
							iy = (ballDY / ballDX) * ((AbsPosX + BoxesFieldGapFromLeft) + (BlocksWidth + BlocksGap) * ia + BlocksHeight - tmpX) + tmpY;
							if (iy > (AbsPosY + BoxesFieldGapFromTop) + (BlocksHeight + BlocksGap) * ib + BlocksHeight) {
								tmpY1 = (AbsPosY + BoxesFieldGapFromTop) + (BlocksHeight + BlocksGap) * ib + BlocksHeight;
								tmpX1 = (ballDX / ballDY) * ((AbsPosY + BoxesFieldGapFromTop) + (BlocksHeight + BlocksGap) * ib + BlocksHeight - tmpY) + tmpX;
								ballX = tmpX1 - 4;
								ballY = tmpY1 - 4;
								ballDY = -ballDY;
							} else {
								if (iy < BlocksHeight + (BlocksHeight + BlocksGap) * ib) {
									tmpY1 = (AbsPosY + BoxesFieldGapFromTop) + (BlocksHeight + BlocksGap) * ib;
									tmpX1 = (ballDX / ballDY) * ((AbsPosY + BoxesFieldGapFromTop) + (BlocksHeight + BlocksGap) * ib - tmpY) + tmpX;
									ballX = tmpX1 - 4;
									ballY = tmpY1 - 4;
									ballDY = -ballDY;
								} else {
									tmpX1 = (AbsPosX + BoxesFieldGapFromLeft) + (BlocksWidth + BlocksGap) * ia + BlocksHeight;
									tmpY1 = (ballDY / ballDX) * ((AbsPosX + BoxesFieldGapFromLeft) + (BlocksWidth + BlocksGap) * ia + BlocksHeight - tmpX) + tmpY;
									ballX = tmpX1 - 4;
									ballY = tmpY1 - 4;
									ballDX = -ballDX;
								}
							}
						}
					}
				}
			}
		}
	}
}

function gameEnd() {
	bgo = true;
	gameFLG = 0;
	loadFLG = 0;
	if (blclr >= 40) {
		//	document.getElementById("submit").style.display = "";
		//	document.getElementById("us").value = gt;
	} else {
		//	document.getElementById("tryagain").style.display = "";
	}
	WinGame();

}

function init() {
	this.strDivHTML = "";
	gt = 0;
	bgo = false;
	for (i = 0; i <= 4; i++) {
		document.images['t' + i].src = "Images/number0.gif";
	}
	//document.getElementById("submit").style.display = "none";
	//document.getElementById("tryagain").style.display = "none";
	write_to_div("<table id='bgIE' width='" + FieldWidth + "' height='" + FieldHeight + "' bgcolor='#000000' onMouseMove='X=event.x;MouseMv();' ><td></td></table>");

	var column = 0;
	var row = 0;
	for (i = 0; i <= 39; i++) {
		write_to_div("<table id='b" + i + "' width='" + BlocksWidth + "' height='" + BlocksHeight + "' bgcolor='" + blcol[row] + "' style='position:absolute;'><td></td></table>");
		column++;
		blsta[i] = row;
		if (column == 8) {
			row++;
			column = 0;
		}
	}

	write_to_div("<div id='ball' style='position:absolute'>");
	write_to_div("<table width='4' height='8' bgcolor='#B0B0B0' style='position:absolute; left:0; top:0'><td></td></table>");
	write_to_div("<table width='8' height='4' bgcolor='#B0B0B0' style='position:absolute; left:-1; top:2'><td></td></table>");
	write_to_div("<table width='4' height='4' bgcolor='#FFFFFF' style='position:absolute; left:0; top:1'><td></td></table>");
	write_to_div("</div>");
	write_to_div("<table id='racket' width='" + RacketWidth + "' height='" + RacketHeight + "' bgcolor='#B0B0FF' style='position:absolute'><td></td></table>");
	write_to_div("<div id='starter' style='position:absolute'><input type='button' value='����� ����' ONCLICK='initG();blur()'></div>");

	document.all['MainBoard'].innerHTML = this.strDivHTML;

	SetAbsPos('MainBoard');
	//alert(AbsPosX+" - "+AbsPosY);

	document.getElementById("ball").style.posTop = AbsPosY + FieldHeight - 25;
	document.getElementById("ball").style.posLeft = AbsPosX + FieldWidth / 2 - 3;
	document.getElementById("racket").style.posTop = AbsPosY + FieldHeight - 15;
	document.getElementById("racket").style.posLeft = AbsPosX + FieldWidth / 2 - 20;
	document.getElementById("starter").style.posTop = AbsPosY + FieldHeight - 83;
	document.getElementById("starter").style.posLeft = AbsPosX + FieldWidth / 2 - 50;

	column = 0;
	row = 0;
	for (i = 0; i <= 39; i++) {
		document.getElementById("b" + i).style.posTop = AbsPosY + 9 + (row * (BlocksHeight + 4));
		document.getElementById("b" + i).style.posLeft = AbsPosX + 8 + (column * (BlocksWidth + 4));
		column++;
		if (column == 8) {
			row++;
			column = 0;
		}
	}

}

function chc(bno, bcl) {
	tmpbno = "b" + bno;
	eval(tmpbno).bgColor = blcol[bcl];
	//	window.status = tmpbno + "-" + bcl + "    " + blclr
}

function write_to_div(str) {
	this.strDivHTML += str;
}

var Size = new Number(7);
var BMoves = new Array();
var noGame = true;
var FD;
var strDivHTML = "";
var gt=0;
var m = 10;
var StartElement = 0;

var Board = new function()
{
	this.init = function()
	{
//	CallToPage('projlib/incs/writetolog.asp','gid='+_gid+'&sid='+_sid +'&msg=function in board init');
		strDivHTML = "";
		write_to_div("<table cellpadding=0 cellspacing=0>");
		for (b = 0; b < Size; b++) {
			write_to_div("<tr>");
			for (a = 0; a < Size; a++) {
				write_to_div("<td><input type='button' style='font-family: monospace; font-weight:bold; font-size: 125%; width: 2em;' value='0' id='"+a+"-"+b+"' onClick=\"buttonMove('"+a+"-"+b+"')\"></td>");
			}
			write_to_div("</tr>");
		}
		write_to_div("</table>");
		document.all['MainBoard'].innerHTML = strDivHTML;
//	CallToPage('projlib/incs/writetolog.asp','gid='+_gid+'&sid='+_sid +'&msg=start element-'+StartElement+'-');
		for(a=0;a<document.forms["Game"].elements.length;a++)
		{
			if (window.document.forms["Game"].elements[a].type=='button')
			{
				StartElement = a;
				a=document.forms["Game"].elements.length;
			}
		}
//	CallToPage('projlib/incs/writetolog.asp','gid='+_gid+'&sid='+_sid +'&msg=start element-'+StartElement+'-');
//		alert(StartElement);
		var ba;
		for(var i = StartElement; i < Size * Size + StartElement; this.set(i++,0));
		for(var i = 0; i < m; i++)
		{
			Board.clicked(ba = Math.floor(Math.random() * Size) + "-" + Math.floor(Math.random() * Size),1);
			Solve.add(ba);
		}
		for(i=0;i<=4;i++){
			document.images['t'+i].src = "Images/number0.gif"
		}
//		document.getElementById("submit").style.display = "none";
	}

	this.clicked = function(square,c)
	{
		var x,y;
		this.change(x = parseInt(square.split("-")[0]),y = parseInt(square.split("-")[1]),c);
		this.change(x - 1,y,c);
  	this.change(x + 1,y,c);
		this.change(x,y - 1,c);
		this.change(x,y + 1,c);
	}

	this.change = function(x,y,c)
	{
		if(this.check(x,y)){
			this.set((x + Size * y) + StartElement,(parseInt(this.get(x,y)) + c) & 3);
		}
	}

	this.set = function(square,val)
	{
//		alert(square+" - "+window.document.forms["form1"].elements[square].id);
//		if (square == 3) window.status = window.document.forms["form1"].elements[square].id;
		window.document.forms["Game"].elements[square].value = val;
	}

	this.get = function(x,y)
	{
		return (this.check(x,y) ? window.document.forms["Game"].elements[(x + Size * y) + StartElement].value : ' ');
	}

	this.check = function(x,y)
	{
		return (x >= 0 && y >= 0 && x < Size && y < Size);
	}
}

var Solve = new function()
{
	this.solve = new Array();
	this.solveID = 0;
	this.init = function()
	{
		this.stop();
		this.solve.length = 0;
		for(var i = 0; i < 9; window.document.forms["hintForm"].elements[i++].value = ' ');
	}

	this.add = function()
	{
		for(var i = 0; i < arguments.length; this.solve.push(arguments[i++]));
		this.solve.sort();
		for(var i = 0; i <= this.solve.length - 4; i++)
			if(this.solve[i] == this.solve[i + 1] && this.solve[i] == this.solve[i + 2] && this.solve[i] == this.solve[i + 3])
				this.solve.splice(i--,4);
		return this.solve.length;
	}

	this.start = function()
	{
		this.solveID = setInterval('Solve.run()',window.document.forms["solveForm"].SolveSpeed.options[window.document.forms["solveForm"].SolveSpeed.selectedIndex].value);
	}

	this.stop = function()
	{
		if(this.solveID)
			clearInterval(this.solveID);
		this.solveID = 0;
	}

	this.run = function()
	{
		var i;
		if(i = this.solve.pop())
			return Board.clicked(i,3);
		this.stop();
	}

	this.hint = function()
	{
		var i = this.solve[Math.floor(Math.random() * this.solve.length)];
		var x = parseInt(i.split("-")[0]);
		var y = parseInt(i.split("-")[1]);
		window.document.forms["hintForm"].elements[0].value = Board.get(x - 1,y - 1);
		window.document.forms["hintForm"].elements[1].value = Board.get(x,y - 1);
		window.document.forms["hintForm"].elements[2].value = Board.get(x + 1,y - 1);
		window.document.forms["hintForm"].elements[3].value = Board.get(x - 1,y);
		window.document.forms["hintForm"].elements[4].value = Board.get(x,y);
		window.document.forms["hintForm"].elements[5].value = Board.get(x + 1,y);
		window.document.forms["hintForm"].elements[6].value = Board.get(x - 1,y + 1);
		window.document.forms["hintForm"].elements[7].value = Board.get(x,y + 1);
		window.document.forms["hintForm"].elements[8].value = Board.get(x + 1,y + 1);
	}
}

var Highscores = new function()
{
	this.highscores = new Array();
	this.init = function()
	{
		for(var i = 0; i < FD.length; i++)
		{
			this.highscores[i] = new Array(10);
			for(var j = 0; j < 10; this.highscores[i][j++] = 1);
		}
	}

	this.add = function(score)
	{
		this.highscores[FD.selectedIndex].push(score);
		this.highscores[FD.selectedIndex].sort();
		this.highscores[FD.selectedIndex].reverse();
		this.highscores[FD.selectedIndex].pop();
		this.show(score);
	}

	this.show = function(score)
	{
		for(var i = 0, t = ''; i < 10; t += (i < 9 ? ' ' : '') + (i + 1) + ': ' + this.highscores[FD.selectedIndex][i] + (score == this.highscores[FD.selectedIndex][i++] ? '  <--' : '') + '\n');
		window.document.forms["scoreForm"].Scores.value = t;
	}
}

function init()
{
//	CallToPage('projlib/incs/writetolog.asp','gid='+_gid+'&sid='+_sid +'&msg=fonction GetScript in init before Board.init');
	Board.init();
	BMoves.length = 0;
	gt = 1;
	noGame = false;
	updateMineTime();
}

function updateMineTime() {
	var _gt = 0;
	var temp = 0;
	if(!noGame && gt < 99999) {
		gt++;
		_gt = gt;
		for (ii=_gt.toString().length - 1;ii>=0;ii--)
		{
			temp = parseInt(_gt / Math.pow(10,ii))
			document.images['t'+ii].src = "Images/number"+temp+".gif"
			_gt = _gt - temp * Math.pow(10,ii)		
		}
		theTimer = window.setTimeout("updateMineTime();",500);
	}
	else{
		clearTimeout(theTimer);
	}
}



function buttonMove(x)
{
	//window.status = x;
	if(noGame)
		return;
	Board.clicked(x,3);
	BMoves.push(x);
	if(Solve.add(x,x,x))
		return;
	noGame = true;
	//document.getElementById("submit").style.display = "";
	if(noGame || gt >= 99999) {
		clearTimeout(theTimer);
	}
	WinGame();
}

function write_to_div(str) {
strDivHTML += str;
}

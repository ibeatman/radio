var timer, i, j, k, best_i, best_j, MoveVal, Start, StartN, Blink, Move, 
MaxMove, IsOver, size = 15, size2 = 225;
IsPlayer = new Array(2);
IsPlayer[0] = 1;
IsPlayer[1] = 0;
StartN = 0;
Fld = new Array(size);
for (i = 0; i < size; i++) {
    Fld[i] = new Array(size);
}
History = new Array(size2);
for (i = 0; i < size2; i++) {
    History[i] = new Array(2);
}
Pic = new Array(3);
Pic[0] = new Image(); Pic[0].src = "images/ttt0.gif";
Pic[1] = new Image(); Pic[1].src = "images/ttt1.gif";
Pic[2] = new Image(); Pic[2].src = "images/ttt2.gif";
function init() {
    var ii, jj;
    MoveVal = 0;
    IsOver = false;
    Blink = 0;
    Start = StartN;
    Move = 0;
    MaxMove = 0;
    for (ii = 0; ii < size; ii++) {
        for (jj = 0; jj < size; jj++)
            Fld[ii][jj] = 0;
    }
    RefreshScreen();
    timer = setInterval("Timer()", 200);
    //   window.document.OptionsForm.Moves.value=" 0 ";
}
function SetOption(nn, mm) {
    if (nn < 2) IsPlayer[nn] = mm;
    else {
        StartN = 1 - mm;
        init();
    }
}
function Timer() {
    if (Blink > 0) {
        Blink--;
        if (Blink > 4) return;
        if (Blink == 0) {
            MakeMove(best_i, best_j, 2);
            RefreshPic(best_i, best_j, MoveVal);
            return;
        }
        if (Blink % 2 == 0) {
            Fld[best_i][best_j] = 2 * ((Move + Start) % 2) - 1;
            RefreshPic(best_i, best_j, 0);
            return;
        }
        if (Blink % 2 == 1) {
            Fld[best_i][best_j] = 0;
            RefreshPic(best_i, best_j, 0);
            return;
        }
    }
    if (!IsOver) {
        if (IsPlayer[(Move + Start) % 2] == 0)
            MakeBestMove();
    }
    else return;
}
function MakeBestMove() {
    var ii, jj, kk, vval, vv_best = -1000, iisvalid = false;
    for (ii = 0; ii < size; ii++) {
        for (jj = 0; jj < size; jj++) {
            for (kk = 0; kk < 2; kk++) {
                vval = MakeMove(ii, jj, kk) - 2 * kk;
                if (vval >= 0) {
                    iisvalid = true;
                    if (vv_best < vval) {
                        vv_best = vval;
                        best_i = ii;
                        best_j = jj;
                    }
                }
            }
        }
    }
    if (iisvalid) Blink = 7;
    else {
        IsOver = true;
        RefreshScreen();
    }
}
function EvalMove(nn, mm, cc) {
    var ii, ll0 = 0, rr0 = 0, hh0 = 0, vv0 = 0, ll1 = 0, rr1 = 0, hh1 = 0, vv1 = 0;
    ii = 1;
    while ((nn - ii >= 0) && (Fld[nn - ii][mm] == cc) && (ii < 5)) ii++;
    vv0 += (ii - 1);
    ii = 1;
    while ((nn + ii < size) && (Fld[nn + ii][mm] == cc) && (ii < 5)) ii++;
    vv1 += (ii - 1);
    if (vv0 + vv1 >= 4) return (2000);
    ii = 1;
    while ((mm - ii >= 0) && (Fld[nn][mm - ii] == cc) && (ii < 5)) ii++;
    hh0 += (ii - 1);
    ii = 1;
    while ((mm + ii < size) && (Fld[nn][mm + ii] == cc) && (ii < 5)) ii++;
    hh1 += (ii - 1);
    if (hh0 + hh1 >= 4) return (2000);
    ii = 1;
    while ((nn - ii >= 0) && (mm - ii >= 0) && (Fld[nn - ii][mm - ii] == cc) && (ii < 5)) ii++;
    ll0 += (ii - 1);
    ii = 1;
    while ((nn + ii < size) && (mm + ii < size) && (Fld[nn + ii][mm + ii] == cc) && (ii < 5)) ii++;
    ll1 += (ii - 1);
    if (ll0 + ll1 >= 4) return (2000);
    ii = 1;
    while ((nn - ii >= 0) && (mm + ii < size) && (Fld[nn - ii][mm + ii] == cc) && (ii < 5)) ii++;
    rr0 += (ii - 1);
    ii = 1;
    while ((nn + ii < size) && (mm - ii >= 0) && (Fld[nn + ii][mm - ii] == cc) && (ii < 5)) ii++;
    rr1 += (ii - 1);
    if (rr0 + rr1 >= 4) return (2000);
    var ll2 = 0, rr2 = 0, hh2 = 0, vv2 = 0, ll3 = 0, rr3 = 0, hh3 = 0, vv3 = 0;
    ii = 1;
    while ((nn - ii >= 0) && (Fld[nn - ii][mm] != -cc) && (ii < 5)) ii++;
    vv2 += (ii - 1);
    ii = 1;
    while ((nn + ii < size) && (Fld[nn + ii][mm] != -cc) && (ii < 5)) ii++;
    vv3 += (ii - 1);
    ii = 1;
    while ((mm - ii >= 0) && (Fld[nn][mm - ii] != -cc) && (ii < 5)) ii++;
    hh2 += (ii - 1);
    ii = 1;
    while ((mm + ii < size) && (Fld[nn][mm + ii] != -cc) && (ii < 5)) ii++;
    hh3 += (ii - 1);
    ii = 1;
    while ((nn - ii >= 0) && (mm - ii >= 0) && (Fld[nn - ii][mm - ii] != -cc) && (ii < 5)) ii++;
    ll2 += (ii - 1);
    ii = 1;
    while ((nn + ii < size) && (mm + ii < size) && (Fld[nn + ii][mm + ii] != -cc) && (ii < 5)) ii++;
    ll3 += (ii - 1);
    ii = 1;
    while ((nn - ii >= 0) && (mm + ii < size) && (Fld[nn - ii][mm + ii] != -cc) && (ii < 5)) ii++;
    rr2 += (ii - 1);
    ii = 1;
    while ((nn + ii < size) && (mm - ii >= 0) && (Fld[nn + ii][mm - ii] != -cc) && (ii < 5)) ii++;
    rr3 += (ii - 1);
    ii = 0;
    if (vv0 + vv1 == 3) {
        if (vv2 > vv0) ii++;
        if (vv3 > vv1) ii++;
    }
    if (hh0 + hh1 == 3) {
        if (hh2 > hh0) ii++;
        if (hh3 > hh1) ii++;
    }
    if (ll0 + ll1 == 3) {
        if (ll2 > ll0) ii++;
        if (ll3 > ll1) ii++;
    }
    if (rr0 + rr1 == 3) {
        if (rr2 > rr0) ii++;
        if (rr3 > rr1) ii++;
    }
    if (ii >= 2) return (1000 + ii);
    if ((vv0 + vv1 >= 1) && (vv2 > vv0) && (vv3 > vv1) && (vv2 + vv3 >= 4)) ii += 20 * (vv0 + vv1);
    if ((hh0 + hh1 >= 1) && (hh2 > hh0) && (hh3 > hh1) && (hh2 + hh3 >= 4)) ii += 20 * (hh0 + hh1);
    if ((ll0 + ll1 >= 1) && (ll2 > ll0) && (ll3 > ll1) && (ll2 + ll3 >= 4)) ii += 20 * (ll0 + ll1);
    if ((rr0 + rr1 >= 1) && (rr2 > rr0) && (rr3 > rr1) && (rr2 + rr3 >= 4)) ii += 20 * (rr0 + rr1);
    if (vv0 + vv3 >= 4) ii += vv0 + 10;
    if (vv1 + vv2 >= 4) ii += vv1 + 10;
    if (hh0 + hh3 >= 4) ii += hh0 + 10;
    if (hh1 + hh2 >= 4) ii += hh1 + 10;
    if (ll0 + ll3 >= 4) ii += ll0 + 10;
    if (ll1 + ll2 >= 4) ii += ll1 + 10;
    if (rr0 + rr3 >= 4) ii += rr0 + 10;
    if (rr1 + rr2 >= 4) ii += rr1 + 10;
    ii += (100 - (nn - 7) * (nn - 7) - (mm - 7) * (mm - 7) + Math.random() * 10) / 100;
    return (ii);
}
function MakeMove(nn, mm, bb) {
    var dd, ii, jj, vv = 0, cc = 2 * ((Move + Start + bb) % 2) - 1;
    if (Fld[nn][mm] != 0) return (-1);
    for (ii = nn - 2; ii <= nn + 2; ii++) {
        for (jj = mm - 2; jj < mm + 2; jj++) {
            if ((ii >= 0) && (ii < size) && (jj >= 0) && (jj < size) && (Fld[ii][jj] != 0)) vv++;
        }
    }
    if (vv > 0) MoveVal = EvalMove(nn, mm, cc);
    else MoveVal = 100 - (nn - 7) * (nn - 7) - (mm - 7) * (mm - 7) + Math.random() * 10;
    if (bb < 2) return (MoveVal);
    Fld[nn][mm] = 2 * ((Move + Start) % 2) - 1;
    if (History[Move][0] != nn) {
        History[Move][0] = nn;
        MaxMove = Move + 1;
    }
    if (History[Move][1] != mm) {
        History[Move][1] = mm;
        MaxMove = Move + 1;
    }
    Move++;
    if (MaxMove < Move) MaxMove = Move;
    return (MoveVal);
}
function Clicked(nn, mm) {
    if (IsPlayer[(Move + Start) % 2]) {
        if (MakeMove(nn, mm, 2) < 0);
        else RefreshPic(nn, mm, MoveVal);
    }
}

function ums() {
	var m = o.intms - o.f;
	if (m >= 0) {
		var m1 = parseInt(m / 10);
		var m0 = m - (m1 * 10)
		document.images['m1'].src = "images/number" + m1 + ".gif"
		document.images['m0'].src = "images/number" + m0 + ".gif"
	}
}


function RefreshPic(ii, jj, vv) {
    if (Fld[ii][jj] == -1) window.document.images["choice" + ii + "_" + jj].src = Pic[1].src;
    if (Fld[ii][jj] == 0) window.document.images["choice" + ii + "_" + jj].src = Pic[0].src;
    if (Fld[ii][jj] == 1) window.document.images["choice" + ii + "_" + jj].src = Pic[2].src;
    //   if (Move<10)
    //     window.document.OptionsForm.Moves.value=" "+Move+" ";
    //   else
    //     window.document.OptionsForm.Moves.value=Move;
    //   window.document.OptionsForm.Moves.blur();
    if (vv >= 2000) {
        IsOver = true;
        if ((Move + Start) % 2 == 0)
            alert("O has won !");
        else
            alert("X has won !");
        clearInterval(timer);
    }
    else {
        if (Move == size2) {
            IsOver = true;
            alert("It's a draw !");
            clearInterval(timer);
        }
    }
}
function RefreshScreen() {
    var ii, jj;
    for (ii = 0; ii < size; ii++) {
        for (jj = 0; jj < size; jj++) {
            if (Fld[ii][jj] == -1) window.document.images["choice" + ii + "_" + jj].src = Pic[1].src;
            if (Fld[ii][jj] == 0) window.document.images["choice" + ii + "_" + jj].src = Pic[0].src;
            if (Fld[ii][jj] == 1) window.document.images["choice" + ii + "_" + jj].src = Pic[2].src;
        }
    }
}
var fiveinrow = "<table bgcolor=#808080>";
for (i = 0; i < size; i++) {
    document.writeln("<tr>");
    for (j = 0; j < size; j++)
        fiveinrow += "<td><a href=\"javascript:Clicked(" + i + "," + j + ")\"><img id='choice" + i + "_" + j + "' src=\"images/ttt0.gif\" style='width:15px'></a></td>";
    fiveinrow += "</tr>";
}
fiveinrow += "</table>";
document.all['MainBoard'].innerHTML = fiveinrow;
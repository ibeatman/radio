function fiveinarow(window, document) {
    var timer, i, j, k, best_i, best_j, MoveVal, Start, StartN, Blink, Move,
        MaxMove, IsOver, size = 15, size2 = 225;
    IsPlayer = new Array(2);
    IsPlayer[0] = 1;
    IsPlayer[1] = 0;
    StartN = 0;
    Fld = new Array(size);
    for (i = 0; i < size; i++) {
        Fld[i] = new Array(size);
    }
    History = new Array(size2);
    for (i = 0; i < size2; i++) {
        History[i] = new Array(2);
    }
    Pic = new Array(5);
    Pic[0] = new Image(); Pic[0].src = "images/ttt0.gif";
    Pic[1] = new Image(); Pic[1].src = "images/ttt1.gif";
    Pic[2] = new Image(); Pic[2].src = "images/ttt2.gif";
    Pic[3] = new Image(); Pic[3].src = "images/ttt3.gif";
    Pic[4] = new Image(); Pic[4].src = "images/ttt4.gif";
    function init() {
        var ii, jj;
        gt = 0;
        MoveVal = 0;
        IsOver = false;
        Blink = 0;
        Start = StartN;
        Move = 0;
        MaxMove = 0;
        for (ii = 0; ii < size; ii++) {
            for (jj = 0; jj < size; jj++)
                Fld[ii][jj] = 0;
        }
        for (i = 0; i <= 4; i++) {
            document.images['t' + i].src = "images/number0.gif"
        }
        RefreshScreen();
        timer = setInterval(Timer, 200);
        clearTimeout(theTimer);
    }
    function SetOption(nn, mm) {
        if (nn < 2) IsPlayer[nn] = mm;
        else {
            StartN = 1 - mm;
            init();
        }
    }
    function Timer() {
        if (Blink > 0) {
            Blink--;
            if (Blink > 4) return;
            if (Blink == 0) {
                MakeMove(best_i, best_j, 2);
                RefreshPic(best_i, best_j, MoveVal);
                return;
            }
            if (Blink % 2 == 0) {
                Fld[best_i][best_j] = 2 * ((Move + Start) % 2) - 1;
                RefreshPic(best_i, best_j, 0);
                return;
            }
            if (Blink % 2 == 1) {
                Fld[best_i][best_j] = 0;
                RefreshPic(best_i, best_j, 0);
                return;
            }
        }
        if (!IsOver) {
            if (IsPlayer[(Move + Start) % 2] == 0)
                MakeBestMove();
        }
        else return;
    }
    function MakeBestMove() {
        var ii, jj, kk, vval, vv_best = -1000, iisvalid = false;
        for (ii = 0; ii < size; ii++) {
            for (jj = 0; jj < size; jj++) {
                for (kk = 0; kk < 2; kk++) {
                    vval = MakeMove(ii, jj, kk) - 2 * kk;
                    if (vval >= 0) {
                        iisvalid = true;
                        if (vv_best < vval) {
                            vv_best = vval;
                            best_i = ii;
                            best_j = jj;
                        }
                    }
                }
            }
        }
        if (iisvalid) Blink = 7;
        else {
            IsOver = true;
            RefreshScreen();
        }
    }
    function EvalMove(nn, mm, cc) {
        var ii, ll0 = 0, rr0 = 0, hh0 = 0, vv0 = 0, ll1 = 0, rr1 = 0, hh1 = 0, vv1 = 0;
        ii = 1;
        while ((nn - ii >= 0) && (Fld[nn - ii][mm] == cc) && (ii < 5)) ii++;
        vv0 += (ii - 1);
        ii = 1;
        while ((nn + ii < size) && (Fld[nn + ii][mm] == cc) && (ii < 5)) ii++;
        vv1 += (ii - 1);
        if (vv0 + vv1 >= 4) return (2000);
        ii = 1;
        while ((mm - ii >= 0) && (Fld[nn][mm - ii] == cc) && (ii < 5)) ii++;
        hh0 += (ii - 1);
        ii = 1;
        while ((mm + ii < size) && (Fld[nn][mm + ii] == cc) && (ii < 5)) ii++;
        hh1 += (ii - 1);
        if (hh0 + hh1 >= 4) return (2000);
        ii = 1;
        while ((nn - ii >= 0) && (mm - ii >= 0) && (Fld[nn - ii][mm - ii] == cc) && (ii < 5)) ii++;
        ll0 += (ii - 1);
        ii = 1;
        while ((nn + ii < size) && (mm + ii < size) && (Fld[nn + ii][mm + ii] == cc) && (ii < 5)) ii++;
        ll1 += (ii - 1);
        if (ll0 + ll1 >= 4) return (2000);
        ii = 1;
        while ((nn - ii >= 0) && (mm + ii < size) && (Fld[nn - ii][mm + ii] == cc) && (ii < 5)) ii++;
        rr0 += (ii - 1);
        ii = 1;
        while ((nn + ii < size) && (mm - ii >= 0) && (Fld[nn + ii][mm - ii] == cc) && (ii < 5)) ii++;
        rr1 += (ii - 1);
        if (rr0 + rr1 >= 4) return (2000);
        var ll2 = 0, rr2 = 0, hh2 = 0, vv2 = 0, ll3 = 0, rr3 = 0, hh3 = 0, vv3 = 0;
        ii = 1;
        while ((nn - ii >= 0) && (Fld[nn - ii][mm] != -cc) && (ii < 5)) ii++;
        vv2 += (ii - 1);
        ii = 1;
        while ((nn + ii < size) && (Fld[nn + ii][mm] != -cc) && (ii < 5)) ii++;
        vv3 += (ii - 1);
        ii = 1;
        while ((mm - ii >= 0) && (Fld[nn][mm - ii] != -cc) && (ii < 5)) ii++;
        hh2 += (ii - 1);
        ii = 1;
        while ((mm + ii < size) && (Fld[nn][mm + ii] != -cc) && (ii < 5)) ii++;
        hh3 += (ii - 1);
        ii = 1;
        while ((nn - ii >= 0) && (mm - ii >= 0) && (Fld[nn - ii][mm - ii] != -cc) && (ii < 5)) ii++;
        ll2 += (ii - 1);
        ii = 1;
        while ((nn + ii < size) && (mm + ii < size) && (Fld[nn + ii][mm + ii] != -cc) && (ii < 5)) ii++;
        ll3 += (ii - 1);
        ii = 1;
        while ((nn - ii >= 0) && (mm + ii < size) && (Fld[nn - ii][mm + ii] != -cc) && (ii < 5)) ii++;
        rr2 += (ii - 1);
        ii = 1;
        while ((nn + ii < size) && (mm - ii >= 0) && (Fld[nn + ii][mm - ii] != -cc) && (ii < 5)) ii++;
        rr3 += (ii - 1);
        ii = 0;
        if (vv0 + vv1 == 3) {
            if (vv2 > vv0) ii++;
            if (vv3 > vv1) ii++;
        }
        if (hh0 + hh1 == 3) {
            if (hh2 > hh0) ii++;
            if (hh3 > hh1) ii++;
        }
        if (ll0 + ll1 == 3) {
            if (ll2 > ll0) ii++;
            if (ll3 > ll1) ii++;
        }
        if (rr0 + rr1 == 3) {
            if (rr2 > rr0) ii++;
            if (rr3 > rr1) ii++;
        }
        if (ii >= 2) return (1000 + ii);
        if ((vv0 + vv1 >= 1) && (vv2 > vv0) && (vv3 > vv1) && (vv2 + vv3 >= 4)) ii += 20 * (vv0 + vv1);
        if ((hh0 + hh1 >= 1) && (hh2 > hh0) && (hh3 > hh1) && (hh2 + hh3 >= 4)) ii += 20 * (hh0 + hh1);
        if ((ll0 + ll1 >= 1) && (ll2 > ll0) && (ll3 > ll1) && (ll2 + ll3 >= 4)) ii += 20 * (ll0 + ll1);
        if ((rr0 + rr1 >= 1) && (rr2 > rr0) && (rr3 > rr1) && (rr2 + rr3 >= 4)) ii += 20 * (rr0 + rr1);
        if (vv0 + vv3 >= 4) ii += vv0 + 10;
        if (vv1 + vv2 >= 4) ii += vv1 + 10;
        if (hh0 + hh3 >= 4) ii += hh0 + 10;
        if (hh1 + hh2 >= 4) ii += hh1 + 10;
        if (ll0 + ll3 >= 4) ii += ll0 + 10;
        if (ll1 + ll2 >= 4) ii += ll1 + 10;
        if (rr0 + rr3 >= 4) ii += rr0 + 10;
        if (rr1 + rr2 >= 4) ii += rr1 + 10;
        ii += (100 - (nn - 7) * (nn - 7) - (mm - 7) * (mm - 7) + Math.random() * 10) / 100;
        return (ii);
    }
    function MakeMove(nn, mm, bb) {
        var dd, ii, jj, vv = 0, cc = 2 * ((Move + Start + bb) % 2) - 1;
        if (Fld[nn][mm] != 0) return (-1);
        for (ii = nn - 2; ii <= nn + 2; ii++) {
            for (jj = mm - 2; jj < mm + 2; jj++) {
                if ((ii >= 0) && (ii < size) && (jj >= 0) && (jj < size) && (Fld[ii][jj] != 0)) vv++;
            }
        }
        if (vv > 0) MoveVal = EvalMove(nn, mm, cc);
        else MoveVal = 100 - (nn - 7) * (nn - 7) - (mm - 7) * (mm - 7) + Math.random() * 10;
        if (bb < 2) return (MoveVal);
        Fld[nn][mm] = 2 * ((Move + Start) % 2) - 1;
        if (History[Move][0] != nn) {
            History[Move][0] = nn;
            MaxMove = Move + 1;
        }
        if (History[Move][1] != mm) {
            History[Move][1] = mm;
            MaxMove = Move + 1;
        }
        if (Move === 0) {
            gt = 1;
            updateMineTime();
        }
        Move++;
        ums();
        if (MaxMove < Move) MaxMove = Move;
        return (MoveVal);
    }
    function Clicked(nn, mm) {
        if (IsPlayer[(Move + Start) % 2]) {
            if (MakeMove(nn, mm, 2) < 0);
            else RefreshPic(nn, mm, MoveVal);
        }
    }

    function ums() {
        var m = Move;
        if (m >= 0) {
            var m1 = parseInt(m / 10);
            var m0 = m - (m1 * 10)
            document.images['m1'].src = "images/number" + m1 + ".gif"
            document.images['m0'].src = "images/number" + m0 + ".gif"
        }
    }

    function showWinning(ii, jj, dir, finds) {
        // console.log(ii, jj, dir, JSON.stringify( finds));
        var lookingFor = Fld[ii][jj];
        var iiNext = 0, jjNext = 0
        if (dir === 1) { // left up
            iiNext = ii - 1;
            jjNext = jj - 1;
        } else if (dir === 2) { // up
            iiNext = ii - 1;
            jjNext = jj;
        } else if (dir === 3) { // right up
            iiNext = ii - 1;
            jjNext = jj + 1;
        } else if (dir === 4) { // right
            iiNext = ii;
            jjNext = jj + 1;
        } else if (dir === 5) { // right down
            iiNext = ii + 1;
            jjNext = jj + 1;
        } else if (dir === 6) { // down
            iiNext = ii + 1;
            jjNext = jj;
        } else if (dir === 7) { // left down
            iiNext = ii + 1;
            jjNext = jj - 1;
        } else if (dir === 8) { // left
            iiNext = ii;
            jjNext = jj - 1;
        }
        if (iiNext >= 0 && jjNext >= 0 && iiNext < 15 && jj < 15 && Fld[iiNext][jjNext] === lookingFor) {
            finds.push([iiNext, jjNext]);
            if (finds.length === 5) {
                var correctImage = lookingFor === 1 ? Pic[3].src : Pic[4].src;
                for (var i = 0; i < finds.length; i++) {
                    window.document.images["choice" + finds[i][0] + "_" + finds[i][1]].src = correctImage;
                }
            }
            else {
                showWinning(iiNext, jjNext, dir, finds);
            }
        }
    }

    function getBeginOfTheRow(ii, jj, dir) {
        var lookingFor = Fld[ii][jj];
        var iiPrev = 0, jjPrev = 0
        if (dir === 5) { // left up
            iiPrev = ii - 1;
            jjPrev = jj - 1;
        } else if (dir === 6) { // up
            iiPrev = ii - 1;
            jjPrev = jj;
        } else if (dir === 7) { // right up
            iiPrev = ii - 1;
            jjPrev = jj + 1;
        } else if (dir === 8) { // right
            iiPrev = ii;
            jjPrev = jj + 1;
        } else if (dir === 1) { // right down
            iiPrev = ii + 1;
            jjPrev = jj + 1;
        } else if (dir === 2) { // down
            iiPrev = ii + 1;
            jjPrev = jj;
        } else if (dir === 3) { // left down
            iiPrev = ii + 1;
            jjPrev = jj - 1;
        } else if (dir === 4) { // left
            iiPrev = ii;
            jjPrev = jj - 1;
        }
        if (iiPrev >= 0 && jjPrev >= 0 && iiPrev < 15 && jj < 15 && lookingFor === Fld[iiPrev][jjPrev]) {
            return getBeginOfTheRow(iiPrev, jjPrev, dir);
        } else {
            return { iiNew: ii, jjNew: jj };
        }
    }

    function RefreshPic(ii, jj, vv) {
        if (Fld[ii][jj] == -1) window.document.images["choice" + ii + "_" + jj].src = Pic[1].src;
        if (Fld[ii][jj] == 0) window.document.images["choice" + ii + "_" + jj].src = Pic[0].src;
        if (Fld[ii][jj] == 1) window.document.images["choice" + ii + "_" + jj].src = Pic[2].src;
        if (vv >= 2000) {
            // console.log('finish');
            for (var i = 1; i < 9; i++) {
                var newPositions = getBeginOfTheRow(ii, jj, i);
                showWinning(newPositions.iiNew, newPositions.jjNew, i, [[newPositions.iiNew, newPositions.jjNew]]);
            }
            IsOver = true;
            clearInterval(timer);
            clearTimeout(theTimer);
        }
        else {
            if (Move == size2) {
                IsOver = true;
                alert("It's a draw !");
                clearInterval(timer);
                clearTimeout(theTimer);
            }
        }
    }
    function RefreshScreen() {
        var ii, jj;
        for (ii = 0; ii < size; ii++) {
            for (jj = 0; jj < size; jj++) {
                if (Fld[ii][jj] == -1) window.document.images["choice" + ii + "_" + jj].src = Pic[1].src;
                if (Fld[ii][jj] == 0) window.document.images["choice" + ii + "_" + jj].src = Pic[0].src;
                if (Fld[ii][jj] == 1) window.document.images["choice" + ii + "_" + jj].src = Pic[2].src;
            }
        }
    }
    var fiveinrow = "<table bgcolor=#808080>";
    for (i = 0; i < size; i++) {
        fiveinrow += "<tr class='row'>";
        for (j = 0; j < size; j++)
            fiveinrow += "<td><img class='choice' id='choice" + i + "_" + j + "' src=\"images/ttt0.gif\" style='width:15px'></td>";
        fiveinrow += "</tr>";
    }
    fiveinrow += "</table>";
    document.all['MainBoard'].innerHTML = fiveinrow;

    const selectors = [].slice.call(document.getElementsByClassName('choice'));
    selectors.forEach(function (element, index) {
        element.addEventListener('click', function (event) {
            var i, j, a;
            a = event.target.id.replace('choice', '').split('_');
            i = a[0];
            j = a[1];
            Clicked(i, j);
        })
    })
    init();
    return init;
};
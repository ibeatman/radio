function initmenu() {
    document.oncontextmenu = showmenu
    if (document.all && window.print)
        document.body.onclick = hidemenu
}

function showmenu() {
    var rightedge = document.body.clientWidth - event.clientX
    var bottomedge = document.body.clientHeight - event.clientY

    if (rightedge < menu.offsetWidth)
        menu.style.left = document.body.scrollLeft + event.clientX - menu.offsetWidth
    else
        menu.style.left = document.body.scrollLeft + event.clientX

    if (bottomedge < menu.offsetHeight)
        menu.style.top = document.body.scrollTop + event.clientY - menu.offsetHeight
    else
        menu.style.top = document.body.scrollTop + event.clientY

    menu.style.visibility = "visible"
    return false
}

function hidemenu() {
    menu.style.visibility = "hidden"
}

function over() {
    if (event.srcElement.className == "menuitems") {
        event.srcElement.style.backgroundColor = "highlight"
        event.srcElement.style.color = "yellow"
        if (display_url == 1)
            window.status = event.srcElement.url
    }
}

function out() {
    if (event.srcElement.className == "menuitems") {
        event.srcElement.style.backgroundColor = ""
        event.srcElement.style.color = "black"
        window.status = ''
    }
}

function linkTo() {
    if (event.srcElement.className == "menuitems")
        window.location = event.srcElement.url
}




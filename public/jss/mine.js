function mine(window, document) {
	var o;

	function init() {
		gt = 0;
		o = new mg(16, 16, 40);
		o.cb();
	}

	function ums() {
		var m = o.intms - o.f;
		if (m >= 0) {
			var m1 = parseInt(m / 10);
			var m0 = m - (m1 * 10)
			document.images['m1'].src = "images/number" + m1 + ".gif"
			document.images['m0'].src = "images/number" + m0 + ".gif"
		}
	}

	function updateMineTime_() {
		var _gt = 0;
		var temp = 0;
		if (!o.bgo && gt < 99999) {
			gt++;
			_gt = gt;
			for (i = _gt.toString().length - 1; i >= 0; i--) {
				temp = parseInt(_gt / Math.pow(10, i))
				document.images['t' + i].src = "images/number" + temp + ".gif"
				_gt = _gt - temp * Math.pow(10, i)
			}
			theTimer = window.setTimeout(updateMineTime, 10);
		}
		else {
			clearTimeout(theTimer);
		}
	}

	function mn() {
		this.ss = "uc";
		this.bim = false;
		this.sm = 0;
	}
	function mg(rs, cs, ms) {
		this.bgo = false;
		this.f = 0;
		this.q = 0;
		this.r = parseInt(rs);
		this.intcs = parseInt(cs);
		this.intms = parseInt(ms);
		this.rss = this.r * this.intcs;
		this.strDivHTML = "";
		this.arrrs = new Array(rs);
		var _root = this;
		this.cb = function () {

			for (var i = 0; i < this.intcs; i++) {
				this.arrrs[i] = new Array(this.intcs);
			}
			for (var i = 0; i < this.r; i++) {
				for (var j = 0; j < this.intcs; j++) {
					this.arrrs[i][j] = new mn();
				}
			}
			for (var i = 0; i < this.intms; i++) {
				var mr = Math.floor(Math.random() * this.r);
				var mc = Math.floor(Math.random() * this.intcs);
				if (!this.arrrs[mr][mc].bim) {
					this.arrrs[mr][mc].bim = true;
				}
				else {
					i--;
				}
			}
			for (var i = 0; i < this.r; i++) {
				for (var j = 0; j < this.intcs; j++) {
					this.strDivHTML += "<img class='mine' src=\"images/unclicked.gif\" name=\"node" + i + "-" + j + "\" id=\"node" + i + "-" + j + "\" border=\"0\" \">";
					for (var k = -1; k < 2; k++) {
						for (var l = -1; l < 2; l++) {
							if (i + k >= 0 && i + k < this.r && j + l >= 0 && j + l < this.intcs) {
								if (this.arrrs[i + k][j + l].bim) {
									this.arrrs[i][j].sm++;
								}
							}
						}
					}
				}
				this.strDivHTML += "<br>";
			}
			document.all['MainBoard'].innerHTML = this.strDivHTML;
			for (i = 0; i <= 4; i++) {
				document.images['t' + i].src = "images/number0.gif"
			}
			const selectors = [].slice.call(document.getElementsByClassName('mine'));
			selectors.forEach(function (element, index) {
				element.addEventListener('click', function (event) {
					_root.cc(event.target.name);
				})
				element.addEventListener('contextmenu', function (event) {
					event.preventDefault();
					_root.rc(event.target.name);
					return false;
				})
			})
			//document.getElementById("submit").style.display = "none"
			//document.getElementById("tryagain").style.display = "none"
			ums();
		}
		this.cc = function (nn) {
			if (!this.bgo) {
				var mr = nn.substring(4, nn.indexOf('-'));
				var mc = nn.substring(nn.indexOf('-') + 1, nn.length);
				if (gt == 0) {
					this.sg();
				}
				if (this.arrrs[mr][mc].ss == "rced") {
					return false;
				}
				else if (this.arrrs[mr][mc].bim) {
					document.images[nn].src = "images/mineclicked.gif";
					this.arrrs[mr][mc].ss = "Boom";
					this.eg(0);
				}
				else if (this.arrrs[mr][mc].sm == 0) {
					this.ds(mr, mc);
				}
				else if (this.arrrs[mr][mc].ss == "d") {
					if (this.mss(mr, mc) == this.arrrs[parseInt(mr)][parseInt(mc)].sm) {
						this.ds(mr, mc)
					}
				}
				else {
					this.arrrs[mr][mc].ss = "d";
					document.images[nn].src = "images/" + this.arrrs[mr][mc].sm + '.gif';
					this.rss--;
					if (this.rss == this.intms) {
						this.eg(1);
					}
				}
			}
		}
		this.rc = function (nn) {
			if (!this.bgo) {
				var mr = nn.substring(4, nn.indexOf('-'));
				var mc = nn.substring(nn.indexOf('-') + 1, nn.length);
				if (this.arrrs[mr][mc].ss == "uc") {
					this.arrrs[mr][mc].ss = "rced";
					document.images[nn].src = "images/flag.gif";
					this.f++;
					ums();
				}
				else if (this.arrrs[mr][mc].ss == "rced") {
					this.arrrs[mr][mc].ss = "rt";
					document.images[nn].src = "images/question.gif";
					this.f--;
					this.q++;
					ums();
				}
				else if (this.arrrs[mr][mc].ss == "rt") {
					this.arrrs[mr][mc].ss = "uc";
					document.images[nn].src = "images/unclicked.gif";
					this.q--;
				}
				else if (this.arrrs[mr][mc].ss == "d") {
					if (this.mss(mr, mc) == this.arrrs[parseInt(mr)][parseInt(mc)].sm) {
						this.ds(mr, mc)
					}
				}
			}
		}
		this.mss = function (ir, ic) {
			var ir = parseInt(ir);
			var ic = parseInt(ic);
			var msn = 0;
			for (var k = -1; k < 2; k++) {
				for (var l = -1; l < 2; l++) {
					if (ir + k >= 0 && ir + k < this.r && ic + l >= 0 && ic + l < this.intcs) {
						if (this.arrrs[parseInt(ir + k)][parseInt(ic + l)].ss == "rced") {
							msn++;
						}
					}
				}
			}
			return msn;
		}
		this.ds = function (ir, ic) {
			var ir = parseInt(ir);
			var ic = parseInt(ic);
			for (var k = -1; k < 2; k++) {
				for (var l = -1; l < 2; l++) {
					if (ir + k >= 0 && ir + k < this.r && ic + l >= 0 && ic + l < this.intcs) {
						if (this.arrrs[parseInt(ir + k)][parseInt(ic + l)].sm == 0 && this.arrrs[parseInt(ir + k)][parseInt(ic + l)].ss != "d") {
							this.arrrs[parseInt(ir + k)][parseInt(ic + l)].ss = "d";
							this.ds(parseInt(ir + k), parseInt(ic + l));
							document.images['node' + parseInt(ir + k) + '-' + parseInt(ic + l)].src = "images/" + this.arrrs[parseInt(ir + k)][parseInt(ic + l)].sm + '.gif';
							this.rss--;
							if (this.rss == this.intms) {
								this.eg(1);
							}
						}
						else {
							if ((this.arrrs[parseInt(ir + k)][parseInt(ic + l)].ss == "rt" || this.arrrs[parseInt(ir + k)][parseInt(ic + l)].ss == "uc") && !this.arrrs[parseInt(ir + k)][parseInt(ic + l)].bim) {
								this.arrrs[parseInt(ir + k)][parseInt(ic + l)].ss = "d";
								document.images['node' + parseInt(ir + k) + '-' + parseInt(ic + l)].src = "images/" + this.arrrs[parseInt(ir + k)][parseInt(ic + l)].sm + '.gif';
								this.rss--;
								if (this.rss == this.intms) {
									this.eg(1);
								}
							}
							else if (this.arrrs[parseInt(ir + k)][parseInt(ic + l)].ss == "rced" && !this.arrrs[parseInt(ir + k)][parseInt(ic + l)].bim) {
								document.images['node' + parseInt(ir + k) + '-' + parseInt(ic + l)].src = "images/nominecheckmine.gif";
								this.eg(0);
							}
							else if (this.arrrs[parseInt(ir + k)][parseInt(ic + l)].ss == "uc" && this.arrrs[parseInt(ir + k)][parseInt(ic + l)].bim) {
								document.images['node' + parseInt(ir + k) + '-' + parseInt(ic + l)].src = "images/mineclicked.gif";
							}
						}
					}
				}
			}
		}
		this.di = function (sm) {
			for (var i = 0; i < this.r; i++) {
				for (var j = 0; j < this.intcs; j++) {
					if (this.arrrs[i][j].bim && this.arrrs[i][j].ss != "Boom") {
						document.images['node' + i + '-' + j].src = "images/" + sm + ".gif";
					}
				}
			}
		}
		this.us = function (nn) {
			var mr = nn.substring(4, nn.indexOf('-'));
			var mc = nn.substring(nn.indexOf('-') + 1, nn.length);
			return "PlayAndWin.co.il";
		}
		this.sg = function () {
			gt = 1;
			updateMineTime();
		}
		this.eg = function (yy) {
			this.bgo = true;
			if (yy) {
				this.di("flag");
				this.f = this.intms;
				ums();
				//			document.getElementById("submit").style.display = "";
				//			document.getElementById("us").value = gt;
			}
			else {
				this.di("mine");
				//			document.getElementById("tryagain").style.display = "";
			}
			clearTimeout(theTimer);
			WinGame();
		}
	}
	init();
	return init;
} (window, document);
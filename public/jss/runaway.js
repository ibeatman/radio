var curX, curY, curX2, curY2, boxX, boxY, moving=0, touch=0;
var gametime=0, started=0, speed;
var starttime, endtime, finaltime=0; //pass finaltime to popup window to ask for initials
var enemyxdir = new Array(1,1,1,1);
var enemyydir = new Array(1,1,1,1);
var random_numberx=0;
var random_numbery=0;
var enemy0move = new Array(random_numberx,random_numbery);
var enemy1move = new Array(random_numberx,random_numbery);
var enemy2move = new Array(random_numberx,random_numbery);
var enemy3move = new Array(random_numberx,random_numbery);
var strDivHTML;
var testing = 0;
var gt = 0;
var bgo = false;
var intGameTime;

// field size
var TablePartSize = 35;
var EnemyHArray = new Array(10,10,30,30);
var EnemyWArray = new Array(30,50,15,10);
var FieldColumn = 11;
var FieldRow = 7;
var FieldWidth = TablePartSize * FieldColumn;
var FieldHeight = TablePartSize * FieldRow;
var BoxSize = 20;

function updateMineTime_() {
	var _gt = 0;
	var temp = 0;
	if(!bgo && gt < 99999) {
		gt++;
		_gt = gt;
		for (i=_gt.toString().length - 1;i>=0;i--)
		{
			temp = parseInt(_gt / Math.pow(10,i))
			document.images['t'+i].src = "Images/number"+temp+".gif"
			_gt = _gt - temp * Math.pow(10,i)		
		}
		window.setTimeout("updateMineTime();",10);
	}
}

function sg() {
	gt = 1;
	updateMineTime();
}
						
document.getElementById("MainBoard").onmousedown = start;
document.getElementById("MainBoard").onmousemove = checkLocation;
document.getElementById("MainBoard").onmouseup = stop;

function giveposX(divname) {
	var posLeft = document.all(divname).style.pixelLeft;
	return posLeft;
}

function giveposY(divname) {
 	var posTop = document.all(divname).style.pixelTop;
	return posTop;
}

function setposX(divname, xpos) {
	document.all(divname).style.pixelLeft = xpos;
}

function setposY(divname, ypos) {
	document.all(divname).style.pixelTop = ypos;
}

function givesize(divname, dimension) {
	var divsize = 0;
		if (dimension == 'y') {
			divsize = document.all(divname).style.pixelHeight;
			}
		else if (dimension == 'x') {
			divsize = document.all(divname).style.pixelWidth;
			}
	return divsize;
}

function checktouching(num) {

	var enemy = "enemy" + num + ""
	var difX = giveposX('box') - giveposX(enemy) - 0; // -0 converts to integer
	var difY = giveposY('box') - giveposY(enemy) - 0;
	
	var Enemysizex = givesize(enemy, 'x');
	var Enemysizey = givesize(enemy, 'y');

	var boxsizex = givesize('box', 'x')
	var boxsizey = givesize('box', 'y')

	if (difX > (-1 * boxsizex) && difX < Enemysizex && difY > (-1 * boxsizey) && difY < Enemysizey) {
		touch = 1;
	}
	else touch = 0;

}

function movenemy(num,step_x,step_y){
	var enemy = "enemy" + num + ""
	var enemyx = givesize(enemy, 'x');
	var enemyy = givesize(enemy, 'y');
	var newposx = giveposX(enemy) + (step_x*enemyxdir[num]) + 0;
	var newposy = giveposY(enemy) + (step_y*enemyydir[num]) + 0;

	//buttom
	if (giveposY(enemy) >= (AbsPosY + FieldHeight - TablePartSize - enemyy)){
		enemyydir[num] = -1 * enemyydir[num];
		newposy = (AbsPosY + FieldHeight - TablePartSize - enemyy) + (step_y*enemyydir[num]);
		}
	//top
	if (giveposY(enemy) <=AbsPosY + 3){
		enemyydir[num] = -1 * enemyydir[num];
		newposy = AbsPosY + 3 + (step_y*enemyydir[num]);
		}
	//right
	if (giveposX(enemy) >= (AbsPosX + FieldWidth - ( 2 * TablePartSize) - enemyx)){
		enemyxdir[num] = -1 * enemyxdir[num];
		newposx = (AbsPosX  + FieldWidth - ( 2 * TablePartSize) - enemyx) + (step_x*enemyxdir[num]);
		}
	//left
	if (giveposX(enemy) <= AbsPosX + 4){
		enemyxdir[num] = -1 * enemyxdir[num];
		newposx = AbsPosX + 4 + (step_x*enemyxdir[num]);
		}

	setposX(enemy, newposx);
	setposY(enemy, newposy);

	checktouching(num + "");
	if (touch == 1 && 0==testing) {
		stop(); reset();
		}
}

function movenemies() {

	if (bgo){
		clearTimeout(intGameTime);
		return;
	}
	gametime = gametime + 1

	if (gametime >= 0 && gametime < 200) speed = 100;
	else if (gametime >= 200 &&  gametime < 300) speed = 80;
	else if (gametime >= 300 &&  gametime < 400) speed = 60;
	else if (gametime >= 400 &&  gametime < 500) speed = 40;
	else if (gametime >= 500 &&  gametime < 600) speed = 20;
	else speed = 10;

	movenemy(0,enemy0move[0],enemy0move[1]);
	movenemy(1,enemy1move[0],enemy1move[1]);
	movenemy(2,enemy2move[0],enemy2move[1]);
	movenemy(3,enemy3move[0],enemy3move[1]);

	intGameTime = setTimeout(movenemies,speed);
}

function start(e) {
	//alert('start');
	if(bgo) return ;
	strDivHTML = "";
	if (started == 0) {	
		movenemies(); 	
		started = 1;	
	}

	curX2 = eval(curX - 20);
	curY2 = eval(curY - 20);

	boxX = eval(curX - 10);
	boxY = eval(curY - 10);

	var boxleft = giveposX('box');
	var boxtop = giveposY('box');

	if (curX > boxleft && curX2 < boxleft && curY > boxtop && curY2 < boxtop) {

		moving = 1;
		setposX('box', boxX);
		setposY('box', boxY);
	}
	sg();
}

function stop(e){
    moving=0;
}

function reset(e){
	moving=0;
	bgo = true;
//		document.getElementById("submit").style.display = "";
//		document.getElementById("us").value = gt;
//		alert();
//			document.location.reload();

//	var OElement = document.getElementById('box');
//  OElement.style.display = "none";
//	for(var iEnemy=0;iEnemy<4;iEnemy++){
//		alert(iEnemy);
//		OElement = document.getElementById('enemy' + iEnemy);
//    OElement.style.display = "none";
//	}
	
	WinGame();
}

function checkLocation(e){
		curX = window.event.x ;
    curY = window.event.y ;

		boxX = eval(curX - 10);
		boxY = eval(curY - 10);

	checktouching('1');

	if (moving == 1 && touch == 0){

			setposX('box',boxX);
			setposY('box',boxY);
			if (curY > AbsPosY + 49 && curX > AbsPosX + 48 && curY < AbsPosY + FieldHeight - BoxSize - TablePartSize + 12 && curX < AbsPosX + FieldWidth - BoxSize - (2 * TablePartSize) - 10 ) return false;
			else {
				stop(); 
				reset();
			}
	}

	else if (touch == 1 && 0==testing){
	stop(); reset();
	}

}

function init(){
//	clearTimeout(intGameTime);
	this.strDivHTML = "";
	gt=0;
	bgo = false;
	for(i=0;i<=4;i++){
		document.images['t'+i].src = "Images/number0.gif";
	}
//	document.getElementById("submit").style.display = "none";
//	document.getElementById("tryagain").style.display = "none";
	moving=0;
	touch=0;
	gametime=0;
	started=0;
	finaltime=0; 
	speed = 100;
 	random_numberx=0;
 	random_numbery=0;
	while(Math.abs(random_numberx) < 3 || Math.abs(random_numbery) < 3 ){
		random_numberx = -20 + Math.random()*40;
		random_numbery = -20 + Math.random()*40;
	}
// 	random_numberx=0;
// 	random_numbery=0;
	enemy0move = new Array(random_numberx,random_numbery);
 	random_numberx=0;
 	random_numbery=0;
	while(Math.abs(random_numberx) < 3 || Math.abs(random_numbery) < 3 ){
		random_numberx = -20 + Math.random()*40;
		random_numbery = -20 + Math.random()*40;
	}
// 	random_numberx=0;
// 	random_numbery=0;
	enemy1move = new Array(random_numberx,random_numbery);
 	random_numberx=0;
 	random_numbery=0;
	while(Math.abs(random_numberx) < 3 || Math.abs(random_numbery) < 3 ){
		random_numberx = -20 + Math.random()*40;
		random_numbery = -20 + Math.random()*40;
	}
// 	random_numberx=0;
// 	random_numbery=0;
	enemy2move = new Array(random_numberx,random_numbery);
 	random_numberx=0;
 	random_numbery=0;
	while(Math.abs(random_numberx) < 3 || Math.abs(random_numbery) < 3 ){
		random_numberx = -20 + Math.random()*40;
		random_numbery = -20 + Math.random()*40;
	}
// 	random_numberx=0;
// 	random_numbery=0;
//alert();
	enemy3move = new Array(random_numberx,random_numbery);

	write_to_div("<div id='box' style='width: "+BoxSize+"px; position: absolute; height: "+BoxSize+"px;background-color: #990000; layer-background-color: #990000'>");
	write_to_div("<table height='"+BoxSize+"px' width='"+BoxSize+"px'>");
	write_to_div("<tr>");
	write_to_div("<td>");
	write_to_div("&nbsp;</td>");
	write_to_div("</tr>");
	write_to_div("</table>");
	write_to_div("</div>");

	for(var iEnemy=0;iEnemy<4;iEnemy++){
		write_to_div("<div id='enemy"+iEnemy+"' style='width: "+EnemyWArray[iEnemy]+"px; position: absolute;");
		write_to_div("height: "+EnemyHArray[iEnemy]+"px; background-color: #000099; layer-background-color: #000099'>");
		write_to_div("<table height='"+EnemyHArray[iEnemy]+"px' width='"+EnemyWArray[iEnemy]+"px'>");
		write_to_div("<tbody>");
		write_to_div("<tr>");
		write_to_div("<td>");
		write_to_div("&nbsp;</td>");
		write_to_div("</tr>");
		write_to_div("</tbody>");
		write_to_div("</table>");
		write_to_div("</div>");
	}

	write_to_div("<table cellspacing='0' cellpadding='0' border='0'>");
	write_to_div("<tbody>");
	
	write_to_div("<tr>");
	for(var iTop=0;iTop<FieldColumn;iTop++){
		PutBlackBox();
	}
	write_to_div("</tr>");
	
	for(var iCenter=0;iCenter<FieldRow - 2;iCenter++){
		write_to_div("<tr>");
		PutBlackBox();
		for(var iiCenter=0;iiCenter<FieldColumn - 2;iiCenter++){
			PutBox();
		}
		PutBlackBox();
		write_to_div("</tr>");
	}

	write_to_div("<tr>");
	for(var iButtom=0;iButtom<FieldColumn;iButtom++){
		PutBlackBox();
	}
	write_to_div("</tr>");
	
	
	write_to_div("</tbody>");
	write_to_div("</table>");
	document.all['MainBoard'].innerHTML = strDivHTML;

//alert();
	SetAbsPos('MainBoard');
//alert();
//	alert(AbsPosX+" - "+AbsPosY);
//	alert(FieldWidth+" - "+FieldHeight);
	document.getElementById("box").style.posTop = AbsPosY + (FieldHeight/2);
	document.getElementById("box").style.posLeft = AbsPosX + (FieldWidth/2);
	
	document.getElementById("enemy0").style.posTop = AbsPosY ;
	document.getElementById("enemy0").style.posLeft = AbsPosX ;
	
	document.getElementById("enemy1").style.posTop = AbsPosY ;
	document.getElementById("enemy1").style.posLeft = AbsPosX + FieldHeight - EnemyHArray[1];
	
	document.getElementById("enemy2").style.posTop = AbsPosY + FieldWidth - EnemyWArray[2] - 170 ;
	document.getElementById("enemy2").style.posLeft = AbsPosX + FieldHeight - EnemyHArray[2] ;
	
	document.getElementById("enemy3").style.posTop = AbsPosY + FieldWidth - EnemyWArray[3] - 170;
	document.getElementById("enemy3").style.posLeft = AbsPosX ;

}
function write_to_div(str) {
strDivHTML += str;
}

function PutBlackBox() {
	write_to_div("<td width='"+TablePartSize+"px' bgcolor='#000000' height='"+TablePartSize+"px'>");
	write_to_div("<table>");
	write_to_div("<tbody>");
	write_to_div("<tr>");
	write_to_div("<td>");
	write_to_div("</td>");
	write_to_div("</tr>");
	write_to_div("</tbody>");
	write_to_div("</table>");
	write_to_div("</td>");
}

function PutBox() {
	write_to_div("<td width='"+TablePartSize+"px' height='"+TablePartSize+"px'>");
	write_to_div("<table>");
	write_to_div("<tbody>");
	write_to_div("<tr>");
	write_to_div("<td>");
	write_to_div("</td>");
	write_to_div("</tr>");
	write_to_div("</tbody>");
	write_to_div("</table>");
	write_to_div("</td>");
}

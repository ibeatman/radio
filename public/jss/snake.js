	var _height = 19; //height of the game area
	var _width = 22;	//width of the game area
	var speed = 100	//speed, higher = slower
	var a = 0;
	var b = 0;
	var di = 0;
	var x = 0;
	var y = 0;
	var n = 0;
	var strDivHTML = "";
	var d = 0;
	var i = 0;
	var ii = 0;
	var end;
	var worm;
	var food;
	var file;
	var o = 0;
	var length = 1;
	var hw ;
	var blank	;
	var tScroll;
	var gt = 0;
	var bgo = false;
	var NumOfNotGameImages = 18

function updateMineTime() {
	var _gt = 0;
	var temp = 0;
	if(!bgo && gt < 99999) {
		gt++;
		_gt = gt;
		for (ii=_gt.toString().length - 1;ii>=0;ii--)
		{
			temp = parseInt(_gt / Math.pow(10,ii))
			document.images['t'+ii].src = "Images/number"+temp+".gif"
			_gt = _gt - temp * Math.pow(10,ii)		
		}
		theTimer = window.setTimeout("updateMineTime();",500);
	}
	else{
		clearTimeout(theTimer);
	}
}

function ums() {
	var m = length;
	if (m >= 0){
		var m1 = parseInt(m / 10);
		var m0 = m - (m1 * 10)
		document.images['m1'].src = "Images/number"+m1+".gif"
		document.images['m0'].src = "Images/number"+m0+".gif"
	}
}
			
	
function keyDown(DnEvents) {
	if(gt==1) {
		updateMineTime();
	}
	k = window.event.keyCode;
	if (k == 37) { d = 1; }
	if (k == 39) { d = 2; }
	if (k == 40) { d = 3; }
	if (k == 38) { d = 4; }
}

function die() {
	bgo = true;
	WinGame();
	clearTimeout(tScroll);
//	document.getElementById("submit").style.display = "";
//	document.getElementById("us").value = gt;
}
function init() {
	di = 0;
	x = 0;
	y = 0;
	n = 0;
	d = 0;
	i = 0;
	ii = 0;
	length = 1;
	speed = 100;
	gt = 0;
	bgo = false;
	strDivHTML = "";
	b=0;
	a=0;
	var imgId = "";
	var ind;
	write_to_div("<table cellpadding=0 cellspacing=0  border='0'><tr><td>");
	for (b = 0; b < _height; b++) {
		for (a = 0; a < _width; a++) {
			if ((b == 0) || (b == _height-1) || (a == 0) || (a == _width-1)) {
				write_to_div("<img id='snake"+b+"-"+a+"' src='images/end.gif' width='0' height='0'>");
			}
			else {
//				write_to_div("<img id='snake"+b+"-"+a+"' alt='snake"+b+"-"+a+"' src='images/blank.gif' width='15' height='15'>");
				write_to_div("<img id='snake"+b+"-"+a+"' src='images/blank.gif' width='15' height='15'>");
	   	}
		}
		write_to_div("<br>");
	}
	write_to_div("</td>");
	write_to_div("</tr></table>");
	document.all['MainBoard'].innerHTML = strDivHTML;

	for(ii=0;ii<=4;ii++){
		document.images['t'+ii].src = "Images/number0.gif"
	}
//	document.getElementById("submit").style.display = "none";
	ums();

	blank = document.images["snake1-1"].src;
	hw = ((_height) * (_width) - 1);
	do {
		o = Math.floor(Math.random() * hw) + NumOfNotGameImages ;
	}while(document.images[getImgId(o,91)].src != blank);

	i = o;
	food = 0;
	
	do {
		food = Math.floor(Math.random() * hw + NumOfNotGameImages);
	} while (document.images[getImgId(food,98)].src != blank);
	
	document.images[getImgId(i,100)].src = "images/worm.gif";
	
	end = document.images["snake0-0"].src;
	file = document.images[getImgId(i,103)].src;
	worm = new Array();
	var k = 0;
	
	document.onkeydown = keyDown;
	gt = 1;
	runTimer();
}
	
function getImgId(Img_Id,line) {
//	alert(Img_Id);
//	alert(document.images[Img_Id].id);
	var	bb = parseInt((Img_Id - NumOfNotGameImages)/ (_width));
	var	aa = parseInt((Img_Id - NumOfNotGameImages) - (bb * (_width)));
	if(bb>=_height || aa>=_width || bb<0 || aa<0 )
	{
//		alert(line);
//		alert(Img_Id);
//		alert("snake"+bb+"-"+aa);
	}
	return "snake"+bb+"-"+aa;
}
	
function runTimer() {
	if (d != 0) { n++; }
	if (d == 1) { i--; }
	if (d == 2) { i++; }
	if (d == 3) { i += _width; }
	if (d == 4) { i -= _width; }
//	window.status = document.images[getImgId(i,135)].src
	if (document.images[getImgId(i,135)].src == end) {
		i = worm[n-1]; 
		di = 1; 
		die();
	}
	worm[n] = i;
	if(i == food) {
		length++; 
		do {
			food = Math.floor(Math.random() * hw) + NumOfNotGameImages ;
//			alert(food);
//			alert(getImgId(food));
		} while (document.images[getImgId(food,145)].src != blank);
		ums();
	}
	if (n > length){
		o = worm[n-length];
	}
	if ((document.images[getImgId(i,150)].src == file) && (n > 1)) {
		speed -= 400; d = 0; di = 1; die();
	}
	if(di == 0) {
		document.images[getImgId(o,154)].src = "images/blank.gif";
		document.images[getImgId(i,155)].src = "images/worm.gif";
		document.images[getImgId(food,156)].src = "images/food.gif";
		tScroll = window.setTimeout("runTimer();", speed);
	}
}

function write_to_div(str) {
	strDivHTML += str;
}

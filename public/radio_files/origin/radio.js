$(document).ready(function() {
    radio = new function() {
        var ua = window.navigator.userAgent.toLowerCase();
        var isIE = ((ua.indexOf('msie') > -1) || (ua.indexOf('trident') > -1) || (ua.indexOf('edge') > -1));
        var sourceOld = '<object id="MediaPlayer1" CLASSID="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701" ' +
            'standby="Loading Microsoft Windows® Media Player components..." type="application/x-oleobject" width="100%" height="302">' +
            '<param name="fileName" value="%station%">' +
            '<param name="animationatStart" value="true">' +
            '<param name="transparentatStart" value="true">' +
            '<param name="autoStart" value="true">' +
            '<param name="showControls" value="true">' +
            '<param name="Volume" value="-450">' +
            '<embed type="application/x-mplayer2" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" src="%station%" name="MediaPlayer1" width=100% height=302 autostart=1 showcontrols=1 volume=-450></embed>' +
            '</object>';

        var source = '<div style="width: auto; margin: 0 auto 50px 0;"><div style="height: 45px;"></div><audio controls autoplay="autoplay" width="100%" src="%station%"></audio></div>';
        var frameSource = '<div style="height: 302px;"><iframe ' +
            'width="100%" ' +
            'height="302px" ' +
            'frameborder="0" ' +
            'src="%station%"' +
            '></iframe></div>';

        var selectedSource = (!!document.createElement('audio').canPlayType && !this.isIE) ? source : sourceOld;


        this.render = function(station, stationTitle) {
            document.getElementById("player").innerHTML = selectedSource.replace(/%station%/g, station);
            document.getElementById("header_flash_news").innerHTML = '<img src="radio_files/radio.png" alt="' + stationTitle + '" title="' + stationTitle + '" style="vertical-align: middle;height: 30px;width: 30px;padding-left: 11px;" /> מנגן עכשיו את תחנת הרדיו : <b>' + stationTitle + '</b>';
            document.title = 'רדיו | רדיו ' + stationTitle + ' | ' + stationTitle;
            $('#content b').html(stationTitle);
            $('#content b').html(stationTitle);
            $('.mainContentInner h2').replaceWith(function () {
                return "<h3>" + $(this).html() + "</h3>";
            })
            this.toPlayer();
        }

        this.toFrame = function() {
            selectedSource = frameSource;
        }

        this.toPlayer = function() {
            selectedSource = (!!document.createElement('audio').canPlayType && !this.isIE) ? source : sourceOld;
        }

    }

    frameRadio = new function() {
        var source = '<iframe ' +
            'width="316" ' +
            'height="300" ' +
            'frameborder="0" ' +
            'src="%station%"' +
            '></iframe>';
        this.render = function(station) {

        }
    }


    if(location.href.indexOf('radio.html') === -1){
        var stationId = location.href.split('/')[location.href.split('/').length - 1];
        var stationLink = $('#' + stationId);
        stationLink.css({
            backgroundColor: "#d4d4d4",
            color: "#D63346"
        });
        var radioCommand = stationLink.attr('radiocommand');
        eval(radioCommand);
    }
    // document.title += ' | ' + $($('#' + location.href.split('/')[location.href.split('/').length - 1]).parent().find('a')[0]).text()

});

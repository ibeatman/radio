var files_path = 'radio_files/',
    ads_files_path = files_path+'c_files/',
    ads_images_path = files_path+'c_images/'
    ads_autoplay = true;

// Query selectors
function $(selector, context){
    var $elem = (context || document).querySelectorAll(selector),
        elemCount = $elem.length;
    if(elemCount == 1){
        $elem = $elem[0];
    }else if(elemCount == 0){
        $elem = false;
    }
    return $elem;
}

(function mainInit(){
    radio = new function() {
        var ua = window.navigator.userAgent.toLowerCase();
        var isIE = ((ua.indexOf('msie') > -1) || (ua.indexOf('trident') > -1) || (ua.indexOf('edge') > -1));
        var sourceOld = '<object id="MediaPlayer1" CLASSID="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701" ' +
            'standby="Loading Microsoft Windows\u00ae Media Player components..." type="application/x-oleobject" width="100%" height="302">' +
            '<param name="fileName" value="%station%">' +
            '<param name="animationatStart" value="true">' +
            '<param name="transparentatStart" value="true">' +
            '<param name="autoStart" value="true">' +
            '<param name="showControls" value="true">' +
            '<param name="Volume" value="-450">' +
            '<embed type="application/x-mplayer2" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" src="%station%" name="MediaPlayer1" width=100% height=302 autostart=1 showcontrols=1 volume=-450></embed>' +
            '</object>';

        var source = '<div style="width: auto; margin: 0 auto 50px 0;"><div style="height: 45px;"></div><audio controls autoplay="autoplay" width="100%" src="%station%"></audio></div>';
        var frameSource = '<div style="height: 302px;"><iframe ' +
            'width="100%" ' +
            'height="302px" ' +
            'frameborder="0" ' +
            'src="%station%"' +
            '></iframe></div>';

        var selectedSource = (!!document.createElement('audio').canPlayType && !this.isIE) ? source : sourceOld;


        this.render = function(station, stationTitle) {
            try{station=com_adswizz_synchro_decorateUrl(station);} catch(e){}
            document.getElementById("header_flash_news").innerHTML = '<img src="'+files_path+'radio.png" alt="' + stationTitle + '" title="' + stationTitle + '" style="vertical-align: middle;height: 30px;width: 30px;padding-left: 11px;" /> \u05de\u05e0\u05d2\u05df \u05e2\u05db\u05e9\u05d9\u05d5 \u05d0\u05ea \u05ea\u05d7\u05e0\u05ea \u05d4\u05e8\u05d3\u05d9\u05d5 : <b>' + stationTitle + '</b>';
            var title = "\u05e8\u05d3\u05d9\u05d5 | \u05e8\u05d3\u05d9\u05d5 " + stationTitle + ' | ' + stationTitle;
            document.title = title;
            $('h1').textContent = title;
			var desc = $('meta[name="description"]').getAttribute("content") + ' ' + title;
			$('meta[name="description"]').setAttribute("content",desc);
            $('#content b').textContent = stationTitle;

            if(skipAdsAllowed){
                document.getElementById("player").innerHTML = selectedSource.replace(/%station%/g, station);
                this.toPlayer();
            }
        }

        this.toFrame = function() {
            if(skipAdsAllowed){
                selectedSource = frameSource;
            }
        }

        this.toPlayer = function() {
            if(skipAdsAllowed){
                selectedSource = (!!document.createElement('audio').canPlayType && !this.isIE) ? source : sourceOld;
            }
        }
    }

    if(location.href.indexOf('radio.html') === -1){
        initAds();
    }

})();


function initAds(){
    window.skipAdsAllowed = false;
    window.$adsPlayer = null;
    window.$playerContainer = document.getElementById('player');
    window.adsDefaultURL = $playerContainer.getAttribute('data-default-url');
    window.adsDefaultImages = $playerContainer.getAttribute('data-default-images').replace(/\[|\]/gi,"").split(',');
    adsDefaultImages = adsDefaultImages[Math.floor(Math.random() * adsDefaultImages.length)].split('|');
    window.skipTime = parseInt($playerContainer.getAttribute('data-ads-skip')) || 10;
    window.vastUrl = $playerContainer.getAttribute('data-vast');
    window.ytVid = $playerContainer.getAttribute('data-youtube-vid');
    window.ytURL = $playerContainer.getAttribute('data-youtube-url');
    window.adsPlayerCurrentTime = 0;
    window.skipCheck = null;

    if(isMobile()){
        ads_autoplay = false;
    }

    emptyPlayerContainer();
    initStation();
    var cookieName = 'radioad';
    var yappCookieName = 'yapp'
    var showAd = getCookie(cookieName);
    var showYapp = getCookie(yappCookieName);

    if(ytVid && ytVid.length == 11 && vastUrl){
        var functions = ['initVast'];
        if(!showAd && !showYapp){
            functions.push('initYoutube');
        }
        var random = functions[Math.floor(Math.random() * functions.length)];
        window[random]();
    }else if(ytVid && ytVid.length == 11 && !showAd && !showYapp){
        initYoutube();
    }else if(vastUrl){
        initVast();
    }else{
        skipAdsAllowed = true;
        initStation();
    }
}

function initYoutube(){
    setCookie(cookieName,1,15);
    window.adsType = 'youtube';

    $playerContainer.classList.add('player-video');

    var ytPlayerElem = document.createElement('div');
        ytPlayerElem.id = 'ytPlayer';
        $playerContainer.appendChild(ytPlayerElem);

    window.$ytOverlay = document.createElement('a');
        $ytOverlay.classList.add('player-overlay');
        $ytOverlay.href = ytURL;
        $ytOverlay.target = '_blank';
        $playerContainer.appendChild($ytOverlay);

    skipButtonSet();

    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

function onYouTubeIframeAPIReady(){
    $adsPlayer = new YT.Player('ytPlayer', {
        height: '1920',
        width: '1080',
        videoId: ytVid,
        playerVars: {wmode: 'transparent', autoplay: ads_autoplay, controls: 0, disablekb: 1, fs: 0, hl: 'he', playsinline: 1, rel: 0, showinfo: 0, iv_load_policy: 3},
        events: {
            'onStateChange': function(event){
                if(skipCheck){clearInterval(skipCheck);}

                if(event.data == 1){
                    skipCheck = setInterval(skipButtonCheck,1000);
                    if($skipAdsButton){$skipAdsButton.classList.add('show');}
                    if($ytOverlay){$ytOverlay.classList.add('show');}
                }

                if(event.data == 0){
                    skipAdsAllowed = true;
                    setCookie(yappCookieName,1,60*24*14);
                    initStation();
                }
            },
            'onError': function(){
                skipAdsAllowed = true;
                initStation();
            }
        }
    });
}

function skipButtonSet(){
    window.$skipAdsButton = document.createElement('button');
    $skipAdsButton.classList.add('player-skip-button');
    $skipAdsButton.innerHTML = "\u05d3\u05dc\u05d2 \u05e4\u05e8\u05e1\u05d5\u05de\u05ea<br/>\u05d1\u05e2\u05d5\u05d3";
    var $skipAdsButtonTime = document.createElement('span');
        $skipAdsButtonTime.textContent = ' '+skipTime;
    $skipAdsButton.appendChild($skipAdsButtonTime);
    $playerContainer.appendChild($skipAdsButton);

    $skipAdsButton.addEventListener('click',function(e){
        e.preventDefault();
            if(skipAdsAllowed){initStation();}
        return false;
    });
}

function skipButtonCheck(){
    if(!$skipAdsButton){return false;}

    if(adsType == 'youtube'){
        adsPlayerCurrentTime = $adsPlayer.getCurrentTime();
    }else if(adsType == 'vast'){
        adsPlayerCurrentTime = $adsPlayer.currentTime();
    }else{
        adsPlayerCurrentTime++;
    }

    adsPlayerCurrentTime = parseInt(Math.floor(adsPlayerCurrentTime));

    $skipAdsButton.lastChild.textContent = ' '+(skipTime - adsPlayerCurrentTime);
    if(adsPlayerCurrentTime >= skipTime){
        skipAdsAllowed = true;
        $skipAdsButton.innerHTML = '\u05d3\u05dc\u05d2 \u05e4\u05e8\u05e1\u05d5\u05de\u05ea';
        $skipAdsButton.classList.add('enabled');
        if(skipCheck){clearInterval(skipCheck);}
    }
}

function initVast(){
    window.adsType = 'vast';
    window.vastScriptsLoaded = 0;

    $playerContainer.classList.add('player-vast');

    var vastStyles = ['video-js.min.css','videojs.vast.vpaid.min.css'];
    for(var i=0; i<vastStyles.length; i++){
        var style = document.createElement('link');
        style.href = ads_files_path+vastStyles[i]+'?1';
        style.rel = 'stylesheet';
        style.type = 'text/css';
        document.head.appendChild(style);
    }

    window.vastScripts = ['video.min.js','videojs_5.vast.vpaid.min.js'];
    var script = document.createElement('script');
    script.src = ads_files_path+vastScripts[0]+'?1';
    script.onload = function(){
        vastScriptsLoaded++;
        var script = document.createElement('script');
        script.src = ads_files_path+vastScripts[1]+'?1';
        script.onload = function(){
            vastScriptsLoaded++;
            if(vastScriptsLoaded == vastScripts.length){
                setVastPlayer();
            }
        };
        document.head.appendChild(script);
    };
    document.head.appendChild(script);
}

function setVastPlayer(){
    if($adsPlayer){$adsPlayer.dispose();}

    var vastPlayerElem = document.createElement('video');
        vastPlayerElem.id = 'vastPlayer';
        vastPlayerElem.classList.add('video-js');
        vastPlayerElem.setAttribute('disableremoteplayback','');
        vastPlayerElem.setAttribute('webkit-playsinline','');
        vastPlayerElem.setAttribute('playsinline','');
        vastPlayerElem.setAttribute('data-vast',vastUrl);
        vastPlayerElem.src = ads_files_path+'vast-loader.mp3?1';
        $playerContainer.appendChild(vastPlayerElem);

    skipButtonSet();

    $adsPlayer = videojs('vastPlayer', {
        controls: true,
        autoplay: ads_autoplay,
        preload: 'auto',
        techOrder: ['html5'],
        controlBar: {fullscreenToggle: false},
        plugins: {
            vastClient: {
                adsEnabled: true,
                playAdAlways: true,
                adTagUrl: vastUrl,
                preferredTech: 'html5',
                vpaidFlashLoaderPath: ads_files_path+'VPAIDFlash.swf'
            }
        }
    }, function(){
        this.on('error', function(){
            skipAdsAllowed = true;
            $adsPlayer.dispose();
            initStation();
        });

        this.on('ready', function(){
            if(ads_autoplay){this.play();}
        });

        this.on('pause', function(){
            if(skipCheck){clearInterval(skipCheck);}
        });

        this.on('play', function(){
            if(skipCheck){clearInterval(skipCheck);}

            skipCheck = setInterval(skipButtonCheck,1000);
            if($skipAdsButton){$skipAdsButton.classList.add('show');}
        });

        this.on('vast.adStart', function(data){
            var image, url;

            data = data.target.player.vast.vastResponse.ads[0].inLine.creatives[1];
            if(data && data.companionAds[0]){
                image = data.companionAds[0].staticResource;
                url = data.companionAds[0].companionClickThrough;
            }else{
                image = ads_images_path+adsDefaultImages[0];
                url = adsDefaultImages[1];
            }
            if(!url){url = adsDefaultURL;}

            if(image){$adsPlayer.poster(image);}
            if(url){
                var container = $('.video-js');
                var before = $('.vjs-control-bar');

                var link = document.createElement('a');
                    link.href = url;
                    link.classList.add('player-ads-link');
                    link.target = '_blank';

                container.insertBefore(link, before);
            }
        });

        this.on('vast.adError', function(){
            skipAdsAllowed = true;
            $adsPlayer.dispose();
            initStation();
        });

        this.on('vast.adSkip', function(){
            skipAdsAllowed = true;
            $adsPlayer.dispose();
            initStation();
        });

        this.on('vast.contentStart', function(){
            skipAdsAllowed = true;
            $adsPlayer.dispose();
            initStation();
        });
    });
}

function emptyPlayerContainer(){
    while($playerContainer.lastChild){$playerContainer.removeChild($playerContainer.lastChild);}
}

function initStation(){
    var stationId = location.href.split('/')[location.href.split('/').length - 1];
    if(stationId.indexOf('station_') === 0){
        stationId = stationId.replace('station_','');
        var links = document.getElementsByClassName('listenTo');
        for (i = 0; i < links.length; ++i) {
            links[i].href = 'station_' + links[i].id;
        }
    }
    var stationLink = document.getElementById(stationId);
    if (stationLink) {
        stationLink.style.backgroundColor = "#d4d4d4";
        stationLink.style.color = "#D63346";

        if(skipAdsAllowed){
            $playerContainer.classList.remove('player-video','player-vast');
            emptyPlayerContainer();
        }

        var radioCommand = stationLink.getAttribute('radiocommand');
        eval(radioCommand);
    }
}

function setCookie(cname, cvalue, exmins) {
    var d = new Date();
    d.setTime(d.getTime() + (exmins*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function isMobile(){
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}

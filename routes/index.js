const express = require('express');
const router = express.Router();
const radio = require('../lib/system/radio-global');
const logger = radio.appContext.logger.getLogger();

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    logger.info('Time: ', Date.now(), req.url);
    next()
})


router.get('/:stationKey', function (req, res, next) {
    radio.appContext.sessionMgr.gateKeeper(req, res, [], {
        success: function (session) {
            var stationKey = req.params.stationKey;
            var stationObj = radio.appContext.siteMap.getStationUIObject(stationKey);
            res.render('radio', stationObj);
        },
        error: function () {
            res.redirect('/login');
        }
    });
});
// router.get('/station/:stationKey', function (req, res, next) {
//     radio.appContext.sessionMgr.gateKeeper(req, res, [], {
//         success: function (session) {
//             var stationKey = req.params.stationKey;
//             var stationObj = radio.appContext.siteMap.getStationUIObject(stationKey);
//             res.render('radio', stationObj);
//         },
//         error: function () {
//             res.redirect('/login');
//         }
//     });
// });


// router.get('/music/:musicKey', function (req, res, next) {
//     radio.appContext.sessionMgr.gateKeeper(req, res, [], {
//         success: function (session) {
//             var ms = require('mediaserver');
//             var AUDIOFILE = "C:/Ronen/Code/MyProjects/myprojects/radioProject/lib/20100614-0729.mp3";
//             ms.pipe(req, res, AUDIOFILE);
//         },
//         error: function () {
//             res.redirect('/login');
//         }
//     });
// });

// router.get('/music2/:musicKey', function (req, res, next) {
//     radio.appContext.sessionMgr.gateKeeper(req, res, [], {
//         success: function (session) {
//             var ms = require('mediaserver');
//             try {
//                 var icecast = require("icecast"); // I'll talk about this module later
//                 // var lame = require("lame");

//                 // var encoder = lame.Encoder({ channels: 2, bitDepth: 16, sampleRate: 44100 });
//                 // encoder.on("data", function (data) {
//                 //     sendData(data);
//                 // });
//                 // var decoder = lame.Decoder();
//                 // decoder.on('format', function (format) {
//                 //     decoder.pipe(encoder);
//                 // });

//                 // var url = 'http://213.8.143.168/91fmAudio';
//                 // var url = "http://firewall.pulsradio.com";
//                 var url = "http://localhost:3000/20100614-0729.mp3";
//                 // var url = "http://103fm.live.streamgates.net/103fm_live/1multix/icecast.audio";

//                 icecast.get(url, function (res) {
//                     res.on('data', function (data) {
//                         // decoder.write(data);
//                         // decoder.write(data);
//                         logger.info(data);
//                         // ms.pipe(req, res, data);

//                         res.pipe(icecast.parse(data));
                        
//                     });
//                 });

//                 // var clients = []; // consider that clients are pushed to this array when they connect

//                 // function sendData(data) {
//                 //     clients.forEach(function (client) {
//                 //         client.write(data);
//                 //     });
//                 // }

// // ================================

//                 // var radio = require("radio-stream");
//                 // var url = "http://67.205.85.183:7714";
//                 // var stream = radio.createReadStream(url);

//             } catch (ex) {
//                 logger.info(ex.message);
//                 var AUDIOFILE = "C:/Ronen/Code/MyProjects/myprojects/radioProject/lib/20100614-0729.mp3";
//                 ms.pipe(req, res, AUDIOFILE);
//             }
//         },
//         error: function () {
//             res.redirect('/login');
//         }
//     });
// });

// /* GET home page. */
// router.get('/home', function (req, res, next) {
//     res.render('nettab');
// });

// router.get('/', function (req, res, next) {
//     radio.appContext.sessionMgr.gateKeeper(req, res, [], {
//         success: function (session) {
//             res.render('oldRadio1');
//         },
//         error: function () {
//             res.redirect('/login');
//         }
//     });
// });
module.exports = router;

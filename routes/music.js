const express = require('express');
const router = express.Router();
const radio = require('../lib/system/radio-global');
const logger = radio.appContext.logger.getLogger();

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    logger.info('Time: ', Date.now(), req.url);
    next()
})

router.get('/station/:stationKey', function (req, res, next) {
    radio.appContext.sessionMgr.gateKeeper(req, res, [], {
        success: function (session) {
            var stationKey = req.params.stationKey;
            var stationObj = radio.appContext.siteMap.getStationUIObject(stationKey);
            res.render('radio', stationObj);
        },
        error: function () {
            res.redirect('/login');
        }
    });
});

// /* GET home page. */
// router.get('/home', function (req, res, next) {
//     res.render('nettab');
// });

module.exports = router;

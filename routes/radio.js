const radio = require('../lib/system/radio-global');

var stationCache = {};

exports.stations = function(req, res) {
    radio.appContext.sessionMgr.gateKeeper(req, res, [], {
        success: function (session) {
            var stationKey = req.params.stationKey;
            if(typeof stationCache[stationKey] == "undefined"){
                stationCache[stationKey] = radio.appContext.siteMap.getStationUIObject(stationKey); 
            }
            stationCache[stationKey].jsSrcs = [
                '/radio_files/radio.min.js',
                //'/radio_files/radio.js',
                '/radio_files/games.min.js',
                // '/jss/general.js',
                // '/jss/fiveinrow.js',
                // '/jss/mine.js'
                // '//p262350.clksite.com/adServe/banners?tid=262350_505480_0&type=floating_banner&size=130x130',
                // '//p262350.clksite.com/adServe/banners?tid=262350_505480_1',
                // '//p262350.clksite.com/adServe/banners?tid=262350_505480_2&type=shadowbox&size=800x440',
                // '//p262350.clksite.com/adServe/banners?tid=262350_505480_3'
            ]
            res.render('radio', stationCache[stationKey]);
        },
        error: function () {
            var stationKey = 'glgltz';
            if(typeof stationCache[stationKey] == "undefined"){
                stationCache[stationKey] = radio.appContext.siteMap.getStationUIObject(stationKey); 
            }
            stationCache[stationKey].jsSrcs = [
                '/radio_files/radio.min.js',
                '/radio_files/games.min.js',
                // '/jss/general.js',
                // '/jss/fiveinrow.js',
                // '/jss/mine.js'
                // '//p262350.clksite.com/adServe/banners?tid=262350_505480_0&type=floating_banner&size=130x130',
                // '//p262350.clksite.com/adServe/banners?tid=262350_505480_1',
                // '//p262350.clksite.com/adServe/banners?tid=262350_505480_2&type=shadowbox&size=800x440',
                // '//p262350.clksite.com/adServe/banners?tid=262350_505480_3'
            ]
            res.render('radio', stationCache[stationKey]);
        }
    });
    
   };
   
   exports.add_hike = function(req, res) {
   };